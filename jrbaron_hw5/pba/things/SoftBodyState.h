//-------------------------------------------------------
//
//  SoftBodyState.h
//  A subclass of DynamicalStateData that handles information on soft body particles. 
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SOFTBODYSTATE_H__
#define __SOFTBODYSTATE_H__

#include "DataStructs.h"  //SoftEdge, etc.
#include "DynamicalState.h"
#include "Vector.h"
#include "Matrix.h"
#include "LinearAlgebra.h"

namespace pba{


//-----------------------------------
class SoftBodyStateData : public DynamicalStateData
{
  public:
    SoftBodyStateData(const std::string& nam = "SoftBodyDataNoName");
    ~SoftBodyStateData();

    std::vector<SoftEdge> getSoftEdges();
    void addSoftEdge(SoftEdge pair);
    void addSoftTri(SoftTriangle tri);
    Vector calcStrutForce(int i, double springStrength, double frictionStrength);
    Vector calcTriAreaForce(int i, double areaStr, double frictionStr);

  private:
    std::vector<SoftEdge> softEdges;
    std::vector<SoftTriangle> softTris;

}; //End class


typedef std::shared_ptr<SoftBodyStateData> SoftBodyState;
SoftBodyState createSoftBodyState( const std::string& nam = "SoftBodyState" );


} //End namespace

#endif
