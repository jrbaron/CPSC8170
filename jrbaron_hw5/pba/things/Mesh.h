//-------------------------------------------------------
//
//  Mesh.h
//  A mesh class to parse an OBJ file and maintain its data.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __MESH_H__
#define __MESH_H__

#include <stdio.h>
#include <stdlib.h>    //malloc(), calloc(), rand()
#include <fcntl.h>     //open(), read(), write(), close()
#include <string.h>    //For atoi() with fgets()

#ifdef __APPLE__
    #include <OpenGL/gl.h>     //For the GLfloat and GLuint types
#else
    #include <GL/gl.h>    
#endif

#include "Vector.h"
#include "DataStructs.h"
#include "SoftBodyState.h"

//----------------------------------------
struct Tri
{ pba::Vector v0, v1, v2; };

struct AABB
{ pba::Vector lowerLeft, upperRight; };

struct KDTreeNode
{
    int level;
    AABB aabb;
    std::vector<Tri> triList;
    KDTreeNode *half0 = NULL; 
    KDTreeNode *half1 = NULL;
};

//----------------------------------------
class Mesh
{
  public:
    Mesh(char *fname, int edges=3);      //Load an OBJ file, or...
    Mesh(int width, int depth,           //Generate a plane along XZ of size width*depth.
        int holeW, int holeD,            //  Can have a hole in the center of size holeW*holeD.
        double edgeLength, int edges, 
        double y);   
    ~Mesh();

    Tri* getTris();
    KDTreeNode* getKDTree();
    pba::Vector* getPbaVectors();
    std::vector<SoftEdge> getSoftEdges();
    GLfloat* getVertexArray();
    bool getIsPlane();
    int getNumFaces();
    int getNumVertices();

    void setStatesSoftConnections(pba::SoftBodyState &state);
    KDTreeNode* getKDNodeOfPoint(pba::Vector pt);
    void printKDTree();

  private:
    bool isPointInAABB(pba::Vector pt, AABB aabb);
    bool isTriInAABB(Tri tri, AABB aabb);
    void splitKDTreeNode(KDTreeNode *node, char axis);
    void makeKDTree();
    void makeTrisForObjMesh();
    void parseObj();

    char *filename;
    int numEdges, numFaces, numVerts, numNormals, numTexCoords;
    GLfloat *vertices, *finalVertices, *finalNormals, *finalTexCoords;
    Tri *triangles;
    pba::Vector *pbaVectors;
    std::vector<SoftEdge> softEdges;
    std::vector<SoftTriangle> softTris;
    bool isPlane;
    KDTreeNode *kdTreeRoot;

};  //End class

#endif 
