//-------------------------------------------------------
//
//  VelPartialSolver.h
//  A partial solver class for velocity.
//     S(v)(dt) {x,v} = {x, (v + f(x,v)/m *dt)}   //Update vel by a timestep of accel.
//  Also implements downward gravity.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class VelPartialSolver
{
  public:
    VelPartialSolver() :
      gravMagnitude(1.0),
      springStrength(1.0),
      frictionStrength(1.0)
    { };

    ~VelPartialSolver() {};

  //--------------------------------------
    void setGravMagnitude(double g)
        { gravMagnitude = g; };

  //--------------------------------------
    void setSpringStrength(double s)
        { springStrength = s; };

  //--------------------------------------
    void setFrictionStrength(double s)
        { frictionStrength = s; };

  //--------------------------------------
    void solve(double dt, Sphere &sphere, Mesh &mesh)
    {
        Vector forces;
        Vector g(0, gravMagnitude*-9.8, 0);

        forces = sphere.mass*g;  
        sphere.vel = sphere.vel + (dt*forces)/sphere.mass;
    };

  //--------------------------------------
    void solveWithSpheres(double dt, std::vector<Sphere> &allSpheres, Mesh &mesh)
    {
        int i, n;
        Sphere sphere;
        Vector forces;
        Vector g(0, gravMagnitude*-9.8, 0);
 
        n = allSpheres.size();
        for(i=0; i<n; i++)
        {
            forces = allSpheres[i].mass * g;
            allSpheres[i].vel = allSpheres[i].vel + ((dt*forces) / allSpheres[i].mass);
        }
    };

  //--------------------------------------
  private:
    double gravMagnitude, springStrength, frictionStrength;

};  //End class

}  //End namespace
