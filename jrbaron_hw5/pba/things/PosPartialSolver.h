//-------------------------------------------------------
//
//  PosPartialSolver.h
//  A partial solver class for position. Determines and handles 
//      collision based on Mesh information passed in. 
//    S(x)(dt) {x,v} = {(x + dt*v), v}       //Update pos by a timestep of vel.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __POSPARTIAL_H__
#define __POSPARTIAL_H__

#include <cmath>
#include <vector>

namespace pba
{

class PosPartialSolver
{
  public:
    PosPartialSolver() :
      restitution(1.0),
      stickiness(1.0)
    { };

    ~PosPartialSolver() {};

  //--------------------------------------
  //Inherited function. Should be called by other solvers 
  //    when getting information down to this level.
  void setRestitution(double r)
     { restitution = r; } 

  //--------------------------------------
  //Inherited function
  void setStickiness(double s)
     { stickiness = s; } 

  //--------------------------------------
  //See if a collision was with the triangle of interest.
    bool detectCollWithTri( Vector xi, Tri tri )
    {
        Vector edge0, edge1, cross0, cross1;
        double a, b, mag0, mag1;           //Barycentric coordinates
        
        edge1 = tri.v2 - tri.v0;
        edge0 = tri.v1 - tri.v0;

        cross0 = edge1^edge0;
        cross1 = edge0^edge1;
        mag0 = cross0.magnitude();
        mag1 = cross1.magnitude();
        
       //^ for cross product, * for dot product
       //No collision if barycentric coords and their sums are not within [0,1] range.
        a = cross0 * (edge1 ^ (xi - tri.v0));
        a = a / (mag0*mag0);
        if ( (a<0) || (a>1) )
            { return false; } 

        b = cross1 * (edge0 ^ (xi - tri.v0));
        b = b / (mag1*mag1);
        if ( (b<0) || (b>1) )
            { return false; } 

        if ( (a+b < 0) || (a+b > 1) )
            { return false; } 

       return true;
    };

  //--------------------------------------
  Vector getTriNormal(Tri tri)
  {
      Vector e0, e1, n;
      e0 = tri.v1 - tri.v0;
      e1 = tri.v2 - tri.v0;
      n = e0 ^ e1;
      n.normalize();
      return n;
  }

  //--------------------------------------
  double checkValidDTs(double dt, double *dti_arr, int numDTs, bool &collision)
  {
      double dti(0.0);
      int i;

      //Check that the collision DTs are less than the full timestep.
      //  If all are valid, dti is set to the one with the largest magnitude.
      //  If one is valid, dti is set to that valid one.
      //  If neither are valid, there is no collision.
      for (i=0; i<numDTs; i++)                            //For each possible dti
      {
          if ( fabs(dti_arr[i]) <= fabs(dt) )      //Not going beyond the timestep
          {
              if ( fabs(dti_arr[i]) > fabs(dti) )  //Larger than the current dti
              {
                  dti = dti_arr[i];
                  collision = true;
              }
          }
      }

     if (dti == 0.0)     //If dti wasn't set.
     { 
         collision = false;  
         return 0.0;
     } 
 
     if (dt * dti < 0.0)   //If not in same direction.
     { 
         collision = false;  
         return 0.0;
     } 

   //printf("dt = %f\n", dt);
   //if (dti != 0.0)
   //printf("\tNew best dti = %f, out of %d\n", dti_arr[i], numDTs);
    return dti;
  }

  //--------------------------------------
  double detectSpherePlaneColl(double dt, Tri tri, Sphere sphere, bool &collision)
  {
      double dti_array[2];
      double dti(0.0), numer, denom;
      Vector np, center_next;
 
      np = getTriNormal(tri);
      center_next = sphere.center + dt*sphere.vel; 

      //Don't divide by zero. Also, if vel*np == 0, then vel is in same dir as normal.
      //   There wouldn't be a collision anyway.
      denom = sphere.vel * np;
      if (denom == 0.0)  
      { 
          //printf("\tdenom is 0\n"); 
          collision = false; 
          return 0.0;
      } 
      else
      { 
          numer = (center_next - tri.v0) * np;
          dti_array[0] = (numer + sphere.radius) / denom;
          dti_array[1] = (numer - sphere.radius) / denom;
         
          dti = checkValidDTs(dt, dti_array, 2, collision);
      } 

      return dti;
  }

  //--------------------------------------
  double detectSphereTriColl(double dt, Tri tri, Sphere sphere, bool &collision)
  {
      double dti_array[12];  //2 possibilities per each of the 3 edges, and 2 per each of the 3 vertices; 6+6=12
      double dti(0.0), q, discr, e_mag, rad;
      double part0, part1, v_perp_mag2, center_perp_mag2, v_mag2;
      int i, j, id;
      Vector edges[3], edge, verts[3], vert, dir;
      Vector v_perp, center_perp, center_i, center_next;

      edges[0] = tri.v1 - tri.v0; 
      edges[1] = tri.v2 - tri.v1;
      edges[2] = tri.v0 - tri.v2;
      verts[0] = tri.v0;
      verts[1] = tri.v1;
      verts[2] = tri.v2;

      center_next = sphere.center + dt*sphere.vel; 
      rad = sphere.radius;
      v_mag2 = sphere.vel.magnitude() * sphere.vel.magnitude();

   //First check sphere collision with all 3 edges; potentially 2 collisions per edge.
      for(i=0; i<6; i+=2)
      {
          id = i/2;
          edge = edges[id];
          e_mag = edge.magnitude();
          dir = center_next - verts[id];   //Direction from pt on plane to sphere

       //The discriminant of the equation for q, where pt on edge x = endpt + q*edge
       //(e dot (center - p0))^2 - (mag(e)^2 * ((center - p0)^2 - R^2) 
          discr = (edge*dir)*(edge*dir) - (e_mag*e_mag)*((dir*dir)-(rad*rad));

          if (discr == 0.0)  //Exactly one solution, then already at collision point.
          {
              collision = true;
              return dt;
          }

          v_perp = sphere.vel - edge*(edge*sphere.vel) / (e_mag*e_mag);
          center_perp = dir - ((edge*(dir*edge)) / (e_mag*e_mag));
          v_perp_mag2 = v_perp.magnitude() * v_perp.magnitude();
          center_perp_mag2 = center_perp.magnitude() * center_perp.magnitude();

          part0 = (-v_perp * center_perp) / v_perp_mag2;
          discr = (v_perp*center_perp)*(v_perp*center_perp) + v_perp_mag2*(center_perp_mag2 - rad*rad);
          part1 = sqrt(discr) / v_perp_mag2;

          dti_array[i] = part0 + part1;
          dti_array[i+1] = part0 - part1;

       //Check that each timestep would result in a point still on the edge.
          for (j=0; j<2; j++)
          {
              center_i = center_next - dti_array[i+j]*sphere.vel;
              q = (edge*(center_i-tri.v0)) / (e_mag*e_mag);
              if ((0.0 > q) || (q > 1.0))  
              {
                  dti_array[i+j] = 0.0;
              }
          }
      }

  //Now check sphere collision with each vertex; again 6 possible dti values.
      for(i=6; i<12; i+=2)  //Pickup i where the edge calculations left off.
      {
          id = (i-6)/2;   //0, 1, or 2
          dir = center_next - verts[id];   //Direction from pt on plane to sphere
          discr = (sphere.vel*dir)*(sphere.vel*dir) - v_mag2*((dir.magnitude()*dir.magnitude())-(rad*rad));

          if (discr == 0.0)  
          {
              collision = true;
              return dt;
          }

          part0 = sphere.vel*dir;
          dti_array[i] = (part0 + discr) / v_mag2;
          dti_array[i+1] = (part0 - discr) / v_mag2;
      }

      dti = checkValidDTs(dt, dti_array, 6, collision);
     // dti = checkValidDTs(dt, dti_array, 12, collision);    //Vertex interaction makes things worse?

      return dti;
  }

  //--------------------------------------
  void solve(double dt, bool useKDTree, Sphere &sphere, Mesh &mesh)
  {
     Vector center_next, np, max_np, vr, center_i;
     double dti_array[2];
     double dti(0.0), max_dti(0.0);
     int numTris(-1), t, d;
     Tri tri, *tris;
     bool collision(true), aCollision(false);   //collision used per triangle; aCollision for the entire set of triangles.

     KDTreeNode *kdNode;
     std::vector<Tri> kdTris;

     center_next = sphere.center + dt*sphere.vel; 

     if (useKDTree)
     {
         kdNode = mesh.getKDNodeOfPoint(center_next);
         if (kdNode != NULL)
         {
             kdTris = kdNode->triList;
             numTris = (int)kdTris.size();
         }
     }
     else
     {
         numTris = mesh.getNumFaces(); 
         tris = mesh.getTris();
     }

     if (numTris != -1)
     {
         for (t=0; t<numTris; t++)
         {
             if (useKDTree)
                 { tri = kdTris[t]; }
             else
                 { tri = tris[t]; }

             collision = false;
             np = getTriNormal(tri);

             dti_array[0] = detectSpherePlaneColl(dt, tri, sphere, collision);
             dti_array[1] = detectSphereTriColl(dt, tri, sphere, collision);  
                                                                          
             if (collision)
             { 
                 for (d=0; d<2; d++)
                 { 
                     if (fabs(dti_array[d]) > fabs(dti))
                         { dti = dti_array[d]; }
                 } 

                 if (fabs(dti) > fabs(max_dti))
                 { 
                     max_dti = dti; 
                     max_np = np;
                     aCollision = true;
                 } 
             } 
         }  //End for tris
      } //end checking numTris
 

     if (aCollision)
     {
         center_i = center_next - max_dti*sphere.vel;
         vr = stickiness*sphere.vel - (stickiness+restitution)*max_np*(max_np*sphere.vel);
         sphere.center = center_i + max_dti*vr;
         sphere.vel = vr;

/*
         printf("\n");
         printf("max_dti=%f\n", max_dti);
         printf("center_i = %f,%f,%f\n", center_i.X(), center_i.Y(), center_i.Z());
         printf("new center = %f,%f,%f\n", sphere.center.X(), sphere.center.Y(), sphere.center.Z());
         printf("vr = %f,%f,%f\n", vr.X(), vr.Y(), vr.Z());
*/
     } 
     else 
     {
         sphere.center = center_next;
         //printf("(no coll) center = %f,%f,%f\n", sphere.center.X(), sphere.center.Y(), sphere.center.Z()); } 
     }

  };

  //--------------------------------------
  void solveWithSpheres(double dt, bool useKDTree, std::vector<Sphere> &allSpheres, Mesh &mesh)
  {
      int i, j, n;
      double radDist, dirMag, discr, velDiffMag;
      double dti_array[2], dti, M;
      Sphere sph_i, sph_j;
      Vector dir, velDiff, v_cm, v_r, norm; 
      bool collision;

      n = allSpheres.size();
      for(i=0; i<n; i++)
      {
          solve(dt, useKDTree, allSpheres[i], mesh);     //First solve with normal triangle collision detection and handling.

          for(j=0; j<n; j++)
          {
            if (j != i)
            {
                sph_i = allSpheres[i];
                sph_j = allSpheres[j];
                dir = sph_i.center - sph_j.center; 
                dirMag = dir.magnitude();
                radDist = (sph_i.radius + sph_j.radius);

                if (dirMag <= radDist)
                {
                   velDiff = sph_j.vel - sph_i.vel;
                   velDiffMag = velDiff.magnitude();
                   if (velDiffMag == 0.0)   //Don't want to divide by zero.
                      { return; }

                   discr = (dir*velDiff) * (dir*velDiff);
                   discr = discr - (4 * (velDiffMag*velDiffMag) * (dirMag*dirMag - radDist*radDist));

                   if (discr < 0.0)    //No solution and therefore no collision.
                      { return; }

                   dti_array[0] = (-(dir*velDiff) + sqrt(discr)) / (velDiffMag*velDiffMag);
                   dti_array[1] = (-(dir*velDiff) - sqrt(discr)) / (velDiffMag*velDiffMag);
                   dti = checkValidDTs(dt, dti_array, 2, collision);

                   if (!collision)
                      { return; }
 
                //Move back to time of collision.
                   sph_i.center = sph_i.center - sph_i.vel*dti;
                   sph_j.center = sph_j.center - sph_j.vel*dti;

                //Calculate reflectance velocities based on the relative velocity.
                   M = sph_i.mass + sph_j.mass;
                   v_cm = (sph_i.mass*sph_i.vel + sph_j.mass*sph_j.vel) / M;
                   norm = dir / dir.magnitude();               //S(i)-S(j) normalized
                   //v_r = velDiff + 2*norm*(norm*velDiff);    //No restitution and stickiness.
                   v_r = stickiness*velDiff - (restitution+stickiness)*norm*(norm*velDiff); 

                   sph_i.vel = v_cm - (sph_j.mass/M)*v_r;   
                   sph_j.vel = v_cm - (sph_i.mass/M)*v_r;

                //Move to the next reflected position. 
                   sph_i.center = sph_i.center + sph_i.vel*dti;
                   sph_j.center = sph_j.center + sph_j.vel*dti;

                   allSpheres[i].vel = sph_i.vel;
                   allSpheres[j].vel = sph_j.vel;
                   allSpheres[i].center = sph_i.center;
                   allSpheres[j].center = sph_j.center;
                }

            }
	  }  //End for all other spheres
      }  //End for all spheres
 
  };

  //--------------------------------------
  private:
    double restitution;
    double stickiness;

};  //End class

}  //End namespace

#endif
