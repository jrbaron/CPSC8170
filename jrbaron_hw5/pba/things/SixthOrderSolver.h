//-------------------------------------------------------
//
//  SixthOrderSolver.h
//  A solver class implementing a Sixth Order approach of Leapfrog. 
//    S6(dt) = S(lf)(dt*a)^2 S(lf)(dt*b) S(lf)(dt*a)^2
//        where a = 1.0/(4-(4^(1.0/3))), b = 1 - 4*a
//
//    Composite solver of partial solvers on position, velocity, position.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#include <cmath>

namespace pba
{

class SixthOrderSolver : public SolverThing
{
  public:
    SixthOrderSolver() :
        SolverThing("SixthOrder"),
        lfSolver( pba::createLeapfrogSolver() ),
        a( 1.0 / (4.0 - pow(4.0, 1.0/3.0)) ),
        b( 1.0 - 4.0*a )
    {

    };

    ~SixthOrderSolver() {};

  //--------------------------------------
  //Inherited method.
  void setGravMagnitude(double g)
     { lfSolver->setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
  void setRestitution(double r)
     { lfSolver->setRestitution(r); }

  //--------------------------------------
  //Inherited method.
  void setStickiness(double s)
     { lfSolver->setStickiness(s); }

  //--------------------------------------
    void solve(double dt, bool useKDTree, Sphere &sphere, Mesh &mesh)
    {
        lfSolver->solve(dt*a, useKDTree, sphere, mesh);
        lfSolver->solve(dt*a, useKDTree, sphere, mesh);
        lfSolver->solve(dt*b, useKDTree, sphere, mesh);
        lfSolver->solve(dt*a, useKDTree, sphere, mesh);
        lfSolver->solve(dt*a, useKDTree, sphere, mesh);
    };

  //--------------------------------------
    void solveWithSpheres(double dt, bool useKDTree, std::vector<Sphere> &allSpheres, Mesh &mesh)
    {
        lfSolver->solveWithSpheres(dt*a, useKDTree, allSpheres, mesh);
        lfSolver->solveWithSpheres(dt*a, useKDTree, allSpheres, mesh);
        lfSolver->solveWithSpheres(dt*b, useKDTree, allSpheres, mesh);
        lfSolver->solveWithSpheres(dt*a, useKDTree, allSpheres, mesh);
        lfSolver->solveWithSpheres(dt*a, useKDTree, allSpheres, mesh);
    };

  //--------------------------------------
  private:
    Solver lfSolver;
    double a, b; 

};  //End class


pba::Solver createSixthOrderSolver()
{
    return Solver( new pba::SixthOrderSolver() );
}

}  //End namespace
