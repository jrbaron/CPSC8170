//-------------------------------------------------------
//
//  SphereSystem.h
//  PbaThing for a system handling sphere-triangle collisions.
//
//  Jessica Baron 
//  CPSC 8170: Homework 5
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define PI 3.14159265  

#include "PbaThing.h"
#include "DynamicalState.h"
#include "DataStructs.h"

#include "Solver.h"
#include "Mesh.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"
#include "PartialTimestepSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif

namespace pba
{


class SphereSystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    SphereSystem(const std::string nam="SphereSystem") :
        PbaThingyDingy (nam),
        solver( pba::createSixthOrderSolver() ),
        ptsPerSphere(400),                         //Only for displaying and should be divisible by 8 (sections of the sphere).
        subTimestep(4),
        allSpheres(),
        allStates(),
        mesh( new Mesh("models/well.obj") ),  
        gravMag(0.6),  
        restitution(1.0), 
        stickiness(1.0),
        useKDTree(false),
        spheresCollide(false)
    {
        std::cout << "Making a " << name << std::endl; 

        dt = 0.01;    //Start small.
        addSphere();
        solver->setGravMagnitude(gravMag);

        std::cout << name << " constructed\n";
    };

    //--------------------------------------------------------
    ~SphereSystem()
    {
        delete mesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {

    }

    //--------------------------------------------------------
    //Make a group of points on the surface of the sphere.
    //These are stored in a DynamicalState per Sphere, used only for displaying.
    void addSphere()
    {
        Sphere sphere;
        DynamicalState state;
        state = pba::CreateDynamicalState();   //Only for displaying.

        int i, j, id;
        double per, random, yellow; 
        Vector dir, pos, sign;
        Vector signs[] = { Vector(1,1,1), Vector(1,1,-1), Vector(1,-1,1), Vector(1,-1,-1),
                           Vector(-1,1,1), Vector(-1,1,-1), Vector(-1,-1,1), Vector(-1,-1,-1) };

        random = (rand()%50)/50.0;
        state->add(ptsPerSphere);	

        sphere.center = pba::Vector(-random, 0.0, random); 
        sphere.vel = pba::Vector(random, -random/4.0, random); 
        sphere.radius = random*0.4;
        sphere.mass = sphere.radius*2.0;

	for (i=0; i<ptsPerSphere; i+=8)
	{
          dir = Vector((rand()%100)/100.0,
                       (rand()%100)/100.0,
                       (rand()%100)/100.0);
          dir.normalize();
          
          for (j=0; j<8; j++)
          {
              id = i+j;

              sign = signs[j];
              dir.set(dir.X()*sign.X(), dir.Y()*sign.Y(), dir.Z()*sign.Z());

              per = (id*1.0)/ptsPerSphere;
              yellow = ((1-per)*0.5)+0.5;

              state->set_id(id, id);
              state->set_pos(id, dir); 
              state->set_ci(id, pba::Color(yellow*0.2, yellow*0.6, random*0.7, 0));
	  }
	}

        allSpheres.push_back(sphere);
        allStates.push_back(state);
    }
   
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Color meshColor(0.4, 0.2, 0.45, 0);
        Vector pos;
        int i, s, numSpheres;
        Sphere sphere;

      //Draw each sphere as a group of points.
        glPointSize(5);
        glBegin(GL_POINTS);
        numSpheres = allSpheres.size();
        for(s=0; s<numSpheres; s++)
        {
            sphere = allSpheres[s]; 
            for(i=0; i<ptsPerSphere; i++)
            {
                pos = allStates[s]->pos(i);  //A direction
                pos.normalize();
                pos = sphere.center + (pos*sphere.radius);	          //Expanded length of radius
                glColor3f( allStates[s]->ci(i).red(), allStates[s]->ci(i).green(), allStates[s]->ci(i).blue() ); 
                glVertex3f(pos.X(), pos.Y(), pos.Z());
            }
        }
        glEnd();

    //Draw mesh to collide with.
        glColor3f( meshColor.red(), meshColor.green(), meshColor.blue() ); 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
        glEnableClientState(GL_VERTEX_ARRAY);
        glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );
    };

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve(), and this class already created a dt.
    void solve()
    {
        int s, numSpheres;
        numSpheres = allSpheres.size();

        if (!spheresCollide)
        {
            for(s=0; s<numSpheres; s++)
               { solver->solve(dt, useKDTree, allSpheres[s], *mesh); }
        }
        else
            { solver->solveWithSpheres(dt, useKDTree, allSpheres, *mesh); }
    };

    //-------------------------------
    void Reset()
    {
        allSpheres.clear();
        allStates.clear();
        addSphere();
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "===" << name << "===\n";
        std::cout << "p:     Print current parameter values.\n";
        std::cout << "a:     Add a sphere to the simulation.\n"; 
        std::cout << "k:     Trigger using the KD-Tree or not in collision detection.\n"; 
        std::cout << "u:     Trigger sphere-sphere collision detection and handling.\n"; 
        std::cout << "g/G:     Reduce/increase magnitude of gravity.\n";
        std::cout << "c/C:     Reduce/increase coefficient of restitution in collisions.\n";
        std::cout << "s/S:     Reduce/increase coefficient of stickiness in collisions.\n";
        std::cout << "b/B:     Reduce/increase the number of substep divisions for Partial Timestep solver.\n";
        std::cout << "1/2/3/4:     Change solver to (1) Leapfrog, (2) Sixth Order using Leapfrog,\n" <<
          "\t\t(3) Partial Timestep using Leapfrog, or (4) Partial Timestep using Sixth Order.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
        PbaThingyDingy::Keyboard(key, x, y);

        if (key == 'a')
        {
            addSphere();
            std::cout << "Added a sphere. Number of spheres = " << allSpheres.size() << ".\n";
        }

        if (key == 'k')
        {
            if (useKDTree)
                { useKDTree = false; } 
            else
                { useKDTree = true; } 
            std::cout << "Using KD-Tree? " << useKDTree << ".\n";
        }

        if (key == 'u')
        {
            if (spheresCollide)
                { spheresCollide = false; } 
            else
                { spheresCollide = true; } 
            std::cout << "Spheres collide? " << spheresCollide << ".\n";
        }

      //Change which solver is being used.
        if (key == '1')
        {
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        {
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }
        else if (key == '3')
        {
            solver = pba::createPartialTimestepSolver( pba::createLeapfrogSolver(), dt, subTimestep );
            Reset();
            std::cout << "Solver changed to Partial Timestep using Leapfrog.\n";
        }
        else if (key == '4')
        {
            solver = pba::createPartialTimestepSolver( pba::createSixthOrderSolver(), dt, subTimestep );
            Reset();
            std::cout << "Solver changed to Partial Timestep using Sixth Order.\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        {
            gravMag -= 0.01;
            if (gravMag <= 0.01) { gravMag = 0.0; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }
        else if (key == 'G')
        {
            gravMag += 0.01;

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }

   //Change stickiness but keep in range [0.0, 1.0].
        if (key == 's')
        {
            stickiness -= 0.01;
            if (stickiness < 0) { stickiness = 0.0; }    //Prevent from being negative.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }
        else if (key == 'S')
        {
            stickiness += 0.01;
            if (stickiness > 1) { stickiness = 1.0; }    //Prevent from exceeding 1.0.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }

     //Change restitution but keep in range [0.0, 1.0].
        if (key == 'c')
        {
            restitution -= 0.01;
            if (restitution < 0) { restitution = 0.0; }    //Prevent from being negative.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }
        else if (key == 'C')
        {
            restitution += 0.01;
            if (restitution > 1) { restitution = 1.0; }    //Prevent from exceeding 1.0.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }

     //Change the timestep division but keep greater than 0.
        if (key == 'b')
        {
            subTimestep -= 1;
            if (subTimestep == 0) { subTimestep = 1; }

            solver->setSubstepDivides(subTimestep);
            std::cout << "Timestep divisions: " << subTimestep << "\n";
        }
        else if (key == 'B')
        {
            subTimestep += 1;

            solver->setSubstepDivides(subTimestep);
            std::cout << "Timestep divisions: " << subTimestep << "\n";
        }

     //Print current user parameters.
        if (key == 'p')
        {
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
            std::cout << "Solver:\t" << solver->getName() << "\n";
            std::cout << "Timestep:\t" << dt << "\n";
            std::cout << "Timestep divisions:\t" << subTimestep << "\n";
            std::cout << "Gravity magnitude:\t" << gravMag << "\n";
            std::cout << "Coeff. of restitution:\t" << restitution << "\n";
            std::cout << "Coeff. of stickiness:\t" << stickiness << "\n";
            std::cout << "Number of spheres:\t" << allSpheres.size() << "\n";
            std::cout << "Using KD-Tree? \t" << useKDTree << "\n";
            std::cout << "Sphere collision on? \t" << spheresCollide << "\n";
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
        }

    };

  //--------------------------------------------------------
  private:
    Solver solver;
    int ptsPerSphere, subTimestep;
    std::vector<Sphere> allSpheres;
    std::vector<DynamicalState> allStates;
    Mesh *mesh;
    double gravMag, velMag, angVelMag; 
    double restitution, stickiness;
    bool useKDTree, spheresCollide;

}; 	//End class declaration



//--------------------------------------------------------
pba::PbaThing makeSphereSystem()
{
    return PbaThing( new pba::SphereSystem() );
}


}  //End namespace pba



#endif
