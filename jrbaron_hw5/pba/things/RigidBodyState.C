
#include "RigidBodyState.h"
#include "LinearAlgebra.h"
#include <iostream>

namespace pba{

//-------------------------------------
RigidBodyStateData::RigidBodyStateData(const std::string &nam) :
    DynamicalStateData(nam),
    totalMass(0.0),
    momentOfInertia(),
    centerOfMassPos(),  //Zero vector
    centerOfMassVel(1.0, 0.0, 0.0), 
    rotationMat(1.0,0.0,0.0,  0.0,1.0,0.0,  0.0,0.0,1.0),  //Identity (no effect yet).
    angularVel()  //Initialized as zero vector (no spinning yet).
  { }

//-------------------------------------
RigidBodyStateData::~RigidBodyStateData()
{ }

//-------------------------------------
//Called by the PosPartialSolver.
void RigidBodyStateData::setCenterOfMassPos(Vector p)
  { 
    //printf("New CoM pos: %f,%f,%f\n", p.X(), p.Y(), p.Z());
    centerOfMassPos = p; 
  }

//-------------------------------------
//Called by the VelPartialSolver.
void RigidBodyStateData::setCenterOfMassVel(Vector v)
  { centerOfMassVel = v; }

//-------------------------------------
//Called by the PosPartialSolver when there's a collision.
void RigidBodyStateData::setAngularVel(Vector v)
  { angularVel = v; }

//-------------------------------------
//Called by the PosPartialSolver when there's a collision.
//Remember to update inertia whenever there's a change to rotation.
void RigidBodyStateData::setRotationMat(Matrix m)
  { 
    rotationMat = m; 
    updateInertia();
  }

//-------------------------------------
double RigidBodyStateData::getTotalMass()
  { return totalMass; }

//-------------------------------------
Vector RigidBodyStateData::getAngularVel()
  { return angularVel; }

//-------------------------------------
Matrix RigidBodyStateData::getRotationMat()
  { return rotationMat; }

//-------------------------------------
Matrix RigidBodyStateData::getMomentOfInertia()
  { return momentOfInertia; }

//-------------------------------------
Vector RigidBodyStateData::getCenterOfMassPos()
  { return centerOfMassPos; }

//-------------------------------------
Vector RigidBodyStateData::getCenterOfMassVel()
  { return centerOfMassVel; }

//-------------------------------------
//Center-of-mass is the guide for both translation and rotation.

//For displaying
// x(i) = x(cm) + R*p(i)
Vector RigidBodyStateData::getVertPos(int i)
{
  return centerOfMassPos + rotationMat*pos(i);
}

//------------------------------
//Position relative to center-of-mass 
//  r(i) = R * p(i)
Vector RigidBodyStateData::getRelVertPos(int i)
{
  return rotationMat*pos(i);
}

//-------------------------------------
void RigidBodyStateData::updateCenterOfMassPos(double dt)
{
  centerOfMassPos += (centerOfMassVel*dt);
}

//-------------------------------------
//With each change to rotation R, compute moment of 
//  inertia I before updating the velocity.
// I(mn) = sum( m(i) * ((mag(r(i))^2)*kroneckerDelta(m,n) - r(i)(m)*r(i)(n)) )
void RigidBodyStateData::updateInertia()
{
  int m, n, i, delta, count; 
  double rMag, total(0.0);
  Vector r;

  count = nb();

  for (m=0; m<3; m++)
  {
    for (n=0; n<3; n++)
    {
    //Function I added in LinearAlgebra.
    //Returns 1 if m==n, 0 otherwise.
      delta = kroneckerDelta(m, n);

      for (i=0; i<count; i++)
      {
        r = getRelVertPos(i);
        rMag = r.magnitude();

        total += mass(i) * (rMag*rMag*delta - r[m]*r[n]);
      }

      momentOfInertia.Set(m, n, total);
      total = 0.0;
    }
  }

}

//-------------------------------------
void RigidBodyStateData::updateRotationMat(double dt)
{
  Vector angVel_norm;
  double mag;

  mag = angularVel.magnitude();
  angVel_norm = angularVel/mag;
//Angle passed into the LinearAlgebra function rot(axis, angle) 
//  needs to be negative.  Positive when going backwards.
  rotationMat = rotation( angVel_norm, -mag*dt ) * rotationMat;
  updateInertia();
}

//-------------------------------------
void RigidBodyStateData::updateCenterOfMassVel(Vector forces, double dt)
{
    //Calculate torque. T = sum( r(i) cross F(i) )
    //If the force is the same on all particles (ex: gravity), torque should be 0.
    Vector torque;
    int i, count;
  
    count = nb();
    for(i=0; i<count; i++)
    {
        torque += (getRelVertPos(i) ^ forces);
    }
    //printf("torque = (%f,%f,%f)\n", torque.X(), torque.Y(), torque.Z());

    centerOfMassVel += ((forces / totalMass)*dt); 
    angularVel += (momentOfInertia.inverse() * torque*dt);
}

//-------------------------------------
void RigidBodyStateData::print()
{
    std::cout << "RigidBodyState params:\n";

    std::cout << "\tTotal mass: " << totalMass << "\n";

    std::cout << "\tMoment of inertia: " <<  
        "\n\t\t" << momentOfInertia.Get(0,0) << ", " << momentOfInertia.Get(0,1) << ", " << momentOfInertia.Get(0,2) << 
        "\n\t\t" << momentOfInertia.Get(1,0) << ", " << momentOfInertia.Get(1,1) << ", " << momentOfInertia.Get(1,2) <<
        "\n\t\t" << momentOfInertia.Get(2,0) << ", " << momentOfInertia.Get(2,1) << ", " << momentOfInertia.Get(2,2) << "\n";
    std::cout << "\tRotation matrix: " <<
        "\n\t\t" << rotationMat.Get(0,0) << ", " << rotationMat.Get(0,1) << ", " << rotationMat.Get(0,2) << 
        "\n\t\t" << rotationMat.Get(1,0) << ", " << rotationMat.Get(1,1) << ", " << rotationMat.Get(1,2) <<
        "\n\t\t" << rotationMat.Get(2,0) << ", " << rotationMat.Get(2,1) << ", " << rotationMat.Get(2,2) << "\n";

    std::cout << "\tCenter of mass position: " << centerOfMassPos.X() << ", " << centerOfMassPos.Y() << ", " << centerOfMassPos.Z() << "\n";
    std::cout << "\tCenter of mass velocity: " << centerOfMassVel.X() << ", " << centerOfMassVel.Y() << ", " << centerOfMassVel.Z() << "\n";
    std::cout << "\tAngular velocity: " << angularVel.X() << ", " << angularVel.Y() << ", " << angularVel.Z() << "\n";

    
}

//-------------------------------------
//Rigid body positions x(i) initialized by system,
//  and also define their masses and compute p(i).
//  p(i) does not change and is used to calculate r(i) (relative pos).
//  Set p(i) just once (not per timestep). 
void RigidBodyStateData::init(double velMag, double angVelMag)
{
  int i, count;
  Vector temp;
  Matrix zeroMat(0.0);
  double a, b, c;

  //Some randomness as {1,2} (for a) or {-1,0,1} 
  //  (for b and c) times a percentage.
  a = ((rand()%2)+1);
  b = ((rand()%3)-1)*0.5;
  c = ((rand()%3)-1)*0.7;

  count = nb();
  totalMass = 0.0;
  angularVel.set(c*angVelMag, a*angVelMag, b*angVelMag);
  centerOfMassVel.set(a*velMag, b*velMag, c*velMag);
  momentOfInertia = zeroMat;

//Update total mass.
  for (i=0; i<count; i++)
    { totalMass += mass(i); }

//Update center of mass.
// x(cm) = sum(m(i)*x(i))/M
// x(i) at this point is the initial position pos(i).
  for (i=0; i<count; i++)
    { temp += (mass(i) * pos(i)); }

  centerOfMassPos = temp/totalMass; 

//Set rotation matrix to identity.
//Constructor does initialize to identity, but this
//  function isn't necessarily called right after
//  the constructor. 
  rotationMat.Set(0, 0, 1.0);
  rotationMat.Set(0, 1, 0.0);
  rotationMat.Set(0, 2, 0.0);
  rotationMat.Set(1, 0, 0.0);
  rotationMat.Set(1, 1, 1.0);
  rotationMat.Set(1, 2, 0.0);
  rotationMat.Set(2, 0, 0.0);
  rotationMat.Set(2, 1, 0.0);
  rotationMat.Set(2, 2, 1.0);
  
//Initialize p(i) = x(i) - x(cm)
  for (i=0; i<count; i++)
  { 
    set_pos( i, pos(i)-centerOfMassPos );
  }

}

} //End namespace

//RigidBodyState type defined in header file.
pba::RigidBodyState pba::createRigidBodyState( const std::string& nam )
{
   return pba::RigidBodyState( new pba::RigidBodyStateData(nam) );
}

