#!/usr/bin/python

import os, sys
import datetime
import pba.swig.PbaViewer as pbav
import pba.swig.PbaThings as things

viewer = pbav.CreateViewer()

args = pbav.StringArray()

for s in sys.argv:
	args.push_back(s)

system = things.makeWindSystem()

now = datetime.datetime.now()
dr = "ScreenCaptures/"+str(now.strftime("%Y-%m-%d_%H:%M:%S"))
if not os.path.isdir(dr):
    os.mkdir(dr)

capture = things.ScreenCapturePPM(dr+"/WindCapture")

viewer.AddThing(system)
viewer.AddThing(capture)

viewer.Init(args)
viewer.MainLoop()

