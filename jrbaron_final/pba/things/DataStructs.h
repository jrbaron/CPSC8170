
#ifndef __STRUCTS_H___
#define __STRUCTS_H___

#include "Vector.h"

//These are in their own file for easier access by multiple classes.

//-----------------------------------
//Used by WindForces class.
struct SinkVortex
{
    double radius, strength;
    pba::Vector center;
};

//-----------------------------------
struct Sphere
{
    double radius, mass;
    pba::Vector center, vel;
};

//-----------------------------------
//2 vertices with an initial distance apart.
struct SoftEdge
{
    int i, j;
    double length;   //Initial length of the edge (dist between the pts)
};

//-----------------------------------
//3 vertices with an initial area.
struct SoftTriangle
{
    int i, j, k; 
    double area;
};

//-----------------------------------
//2 triangles with a shared edge (and remember they're in 3D space).
//(Currently unused in HW4.)
struct SoftBendable
{
    int i, j, k, l;
    double theta;    //Initial theta from crossing the normals of the left and right triangles.
};

//-----------------------------------

#endif
