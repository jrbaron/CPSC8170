//-------------------------------------------------------
//
//  VelPartialSolver.h
//  A partial solver class for velocity.
//     S(v)(dt) {x,v} = {x, (v + f(x,v)/m *dt)}   //Update vel by a timestep of accel.
//  Also implements downward gravity.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#include "WindForces.h"

namespace pba
{

class VelPartialSolver
{
  public:
    VelPartialSolver() :
      gravMagnitude(1.0),
      airResist(1.0),
      windForces()
    { };

    ~VelPartialSolver() {};

  //--------------------------------------
    void setWindForcesStrengths(double s, double v)
    {
        windForces.setSourceSinkStrength(s); 
        windForces.setVortexStrength(v); 
    };

  //--------------------------------------
    void setWindRadius(double r)
        { windForces.setRadius(r); };

  //--------------------------------------
    void setGravMagnitude(double g)
        { gravMagnitude = g; };

  //--------------------------------------
    void setAirResistRoughness(double r)
        { airResist = r; };

  //--------------------------------------
    void solveRB(double dt, RigidBodyState &rbState, Mesh &mesh)
    {
        Vector frc, windVel, pos;
        Vector g(0, gravMagnitude*-9.8, 0);     
        double m;

        m = rbState->getTotalMass();
        frc = m*g;

        pos = rbState->getCenterOfMassPos();
	windVel = windForces.getWindVel(pos);
 //     printf("Wind vel: %f,%f,%f\n", windVel.X(), windVel.Y(), windVel.Z());
	frc += (airResist*windVel);

        rbState->updateCenterOfMassVel(frc, dt);
    };

  //--------------------------------------
  private:
    double gravMagnitude;
    double airResist;
      //Air resistance roughness is a constant determining how much
      //  the object is affected by air resistance / wind.
      //  Depends on size of object.
    WindForces windForces;

};  //End class

}  //End namespace
