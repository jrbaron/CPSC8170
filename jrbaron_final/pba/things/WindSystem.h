//-------------------------------------------------------
//
//  WindSystem.h
//  PbaThing for a system implementing wind-like forces through 
//    on a rigid body.  The user can control several different
//    strengths affecting the wind.
//
//  Jessica Baron 
//  CPSC 8170: Homework 3
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define PI 3.14159265  

#include "PbaThing.h"
#include "RigidBodyState.h"
#include "SoftBodyState.h"

#include "Solver.h"
#include "DataStructs.h"
#include "Mesh.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif

namespace pba
{


class WindSystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    WindSystem(const std::string nam="WindSystem") :
        PbaThingyDingy (nam),
        solver( pba::createSixthOrderSolver() ),
        rbState( pba::createRigidBodyState(nam) ),
        collMeshState( pba::createSoftBodyState("Collision mesh's state") ),

        mesh( new Mesh("models/larger/Cube_large.obj", 3) ),   
        rbMesh( new Mesh("models/ekp_tris.obj", 3) ),   
       // rbMesh( new Mesh("models/smaller/smallsphere.obj", 3) ),  
       // rbMesh( new Mesh("models/smaller/thinRectPrism.obj", 3) ),  
       // rbMesh( new Mesh("models/smaller/torus.obj", 3) ),  

        numParticles(-1),      //Should be set by the mesh.
        gravMag(0.02),    
        velMag(1.0),
        angVelMag(0.1),
        restitution(0.0), 
        stickiness(0.2),
        airResist(50.0),          //The wind should affect the rigid body a lot.
        windBaseRadius(3.0),      //Size of the sinks/sources and vortices in WindForces class object.
        sinkSourceStrength(0.0),  //Starting out as no effect because negative values cause a sink and positive, a source. Let user decide.
        vortexStrength(10.0)
    {
        std::cout << "Making a " << name << std::endl; 

        solver->setGravMagnitude(gravMag);
        solver->setAirResistRoughness(airResist);

        int num;
        if (mesh->getIsPlane())
        {
           num = mesh->getNumVertices();
           collMeshState->add(num);
           mesh->setStatesSoftConnections(collMeshState);
           setupSoftState(collMeshState, mesh, 2.0);
        }

        numParticles = rbMesh->getNumVertices();
        rbState->add(numParticles);
        setupRigidState();

        std::cout << name << " constructed\n";
    };

    //--------------------------------------------------------
    ~WindSystem()
    {
        delete mesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {

    }

    //--------------------------------------------------------
    //c alters color percentages.
    //Need separate rigid- and soft-body state methods because of casting issues with shared_ptrs.
    //void setupRigidState(pba::RigidBodyState &s, Mesh *m, double c)
    void setupRigidState()
    {
        int i;
        double per, random, yellow; 
        Vector *meshVerts;

        meshVerts = rbMesh->getPbaVectors();

        random = (rand()%100)/100.0;
        for (i=0; i<numParticles; i++)
        {
            per = (i*1.0)/numParticles;
            yellow = ((1-per)*0.5)+0.5;
            rbState->set_id(i, i);
            rbState->set_pos(i, meshVerts[i]);	
            rbState->set_mass(i, 1.0);
            rbState->set_ci(i, pba::Color(random*0.3, yellow*0.7, yellow*2, 0));
        }

        rbState->init(velMag, angVelMag);
    }

    //--------------------------------------------------------
    void setupSoftState(pba::SoftBodyState &s, Mesh *m, double c)
    {
        int i, cnt;
        double per, random, yellow; 
        Vector *meshVerts;

        cnt = m->getNumVertices();
        meshVerts = m->getPbaVectors();

        random = (rand()%100)/100.0;
        for (i=0; i<cnt; i++)
        {
            per = (i*1.0)/cnt;
            yellow = ((1-per)*0.5)+0.5;
            s->set_id(i, i);
            s->set_pos(i, meshVerts[i]);	
            s->set_vel(i, Vector());	
            s->set_mass(i, 1.0);
            s->set_ci(i, pba::Color(random*c*0.3, yellow*c*0.7, yellow*c*2, 0));
        }

    }
 
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Color meshColor(0.85, 0.8, 0.9, 0);
        Vector pos;
	std::vector<SoftEdge> pairs;
	SoftEdge pair;
        RigidBodyState s;
	int i, j, p, cnt;

    //Draw particles as points.
        glPointSize(5.0);
        glBegin(GL_POINTS);
        for(i=0; i<numParticles; i++)
        {
            pos = rbState->getVertPos(i);
      
            glColor3f( rbState->ci(i).red(), rbState->ci(i).green(), rbState->ci(i).blue() ); 
            glVertex3f( pos.X(), pos.Y(), pos.Z() );
        }
        glEnd();

    //Draw mesh to collide with.
        if (mesh->getIsPlane())
        {
           pairs = collMeshState->getSoftEdges();
           cnt = pairs.size();
           glBegin(GL_LINES);
           for(p=0; p<cnt; p++)
           {
               pair = pairs[p];
               i = pair.i;
               j = pair.j;
               glColor3f( collMeshState->ci(i).red(), collMeshState->ci(i).green(), collMeshState->ci(i).blue() ); 
               pos = collMeshState->pos(pair.i);
               glVertex3f( pos.X(), pos.Y(), pos.Z() );
               pos = collMeshState->pos(pair.j);
               glVertex3f( pos.X(), pos.Y(), pos.Z() );
           }
           glEnd();
       }
       else   //if loaded from OBJ
       {
           glColor3f( meshColor.red(), meshColor.green(), meshColor.blue() ); 
           glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
           glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
           glEnableClientState(GL_VERTEX_ARRAY);
           glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );
       }

    };

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve(), and this class already created a dt.
    void solve()
    {
        solver->solve(dt, rbState, *mesh);
    };

    //-------------------------------
    void Reset()
    { 
        setupRigidState(); 
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "===" << name << "===\n";
        std::cout << "p:     Print current parameter values.\n";
        std::cout << "v/V:   Decrease/increase the magnitude of the initial velocity.\n";
        std::cout << "a/A:   Decrease/increase the magnitude of the initial angular velocity.\n";
        std::cout << "g/G:     Reduce/increase magnitude of gravity.\n";
        std::cout << "c/C:     Reduce/increase coefficient of restitution in collisions.\n";
        std::cout << "s/S:     Reduce/increase coefficient of stickiness in collisions.\n";
        std::cout << "w/W:     Reduce/increase coefficient of air resistant roughness (low values mean not affected by wind much).\n";
        std::cout << "j/J:     Reduce/increase strength of wind sinks (if negative) or sources (if positive).\n";
        std::cout << "k/K:     Reduce/increase strength of wind vortices.\n";
        std::cout << "l/L:     Reduce/increase base radius of wind spheres.\n";
        std::cout << "1/2:     Change solver to (1) Leapfrog or (2) Sixth Order using Leapfrog.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
        PbaThingyDingy::Keyboard(key, x, y);

        if (key == 'v')
        {
            velMag -= 0.01;
            std::cout << "Initial velocity magnitude: " << velMag << ".  Reset ('r') to see changes.\n";
        }
        else if (key == 'V')
        {
            velMag += 0.01;
            std::cout << "Initial velocity magnitude: " << velMag << ".  Reset ('r') to see changes.\n";
        }

        if (key == 'a')
        {
            angVelMag -= 0.01;
            std::cout << "Initial angular velocity magnitude: " << angVelMag << ".  Reset ('r') to see changes.\n";
        }
        else if (key == 'A')
        {
            angVelMag += 0.01;
            std::cout << "Initial angular velocity magnitude: " << angVelMag << ".  Reset ('r') to see changes.\n";
        }

      //Change which solver is being used.
        if (key == '1')
        { 
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        { 
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        {
            gravMag -= 0.01;
            if (gravMag <= 0.01) { gravMag = 0.0; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }
        else if (key == 'G')
        {
            gravMag += 0.01;

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }

   //Change stickiness but keep in range [0.0, 1.0].
        if (key == 's')
        {
            stickiness -= 0.01;
            if (stickiness < 0) { stickiness = 0.0; }    //Prevent from being negative.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }
        else if (key == 'S')
        {
            stickiness += 0.01;
            if (stickiness > 1) { stickiness = 1.0; }    //Prevent from exceeding 1.0.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }

     //Change restitution but keep in range [0.0, 1.0].
        if (key == 'c')
        {
            restitution -= 0.01;
            if (restitution < 0) { restitution = 0.0; }    //Prevent from being negative.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }
        else if (key == 'C')
        {
            restitution += 0.01;
            if (restitution > 1) { restitution = 1.0; }    //Prevent from exceeding 1.0.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }

     //Change air resistance roughness of the rigid body but keep positive.
        if (key == 'w')
        {
            airResist -= 1.0;
            if (airResist < 0) { airResist = 0.0; }    //Prevent from being negative.

            solver->setAirResistRoughness(airResist);
            std::cout << "Coeff. of air resistance roughness: " << airResist << "\n";
        }
        else if (key == 'W')
        {
            airResist += 1.0;

            solver->setAirResistRoughness(airResist);
            std::cout << "Coeff. of air resistance roughness: " << airResist << "\n";
        }

     //Change strength of sinks/sources in the WindForces. 
        if (key == 'j')
        {
            sinkSourceStrength -= 0.1;

            solver->setWindForcesStrengths(sinkSourceStrength, vortexStrength);
            std::cout << "Strength of wind sinks/sources: " << sinkSourceStrength << "\n";
        }
        else if (key == 'J')
        {
            sinkSourceStrength += 0.1;

            solver->setWindForcesStrengths(sinkSourceStrength, vortexStrength);
            std::cout << "Strength of wind sinks/sources: " << sinkSourceStrength << "\n";
        }

     //Change strength of vortices in the WindForces. 
        if (key == 'k')
        {
            vortexStrength -= 0.1;

            solver->setWindForcesStrengths(sinkSourceStrength, vortexStrength);
            std::cout << "Strength of wind vortices: " << vortexStrength << "\n";
        }
        else if (key == 'K')
        {
            vortexStrength += 0.1;

            solver->setWindForcesStrengths(sinkSourceStrength, vortexStrength);
            std::cout << "Strength of wind vortices: " << vortexStrength << "\n";
        }

     //Change base radius used for the sphere-like WindForce objects (sink/source and vortices). 
        if (key == 'l')
        {
            windBaseRadius -= 0.5;
            if (windBaseRadius < 0) { windBaseRadius = 0.0; }    //Prevent from being negative.

            solver->setWindRadius(windBaseRadius);
            std::cout << "Base wind sphere radius: " << windBaseRadius << "\n";
        }
        else if (key == 'L')
        {
            windBaseRadius += 0.5;

            solver->setWindRadius(windBaseRadius);
            std::cout << "Base wind sphere radius: " << windBaseRadius << "\n";
        }

     //Print current user parameters.
        if (key == 'p')
        {
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
            std::cout << "Initial velocities magnitude: " << velMag << "\n";
            std::cout << "Gravity magnitude:\t" << gravMag << "\n";
            std::cout << "Coeff. of restitution:\t" << restitution << "\n";
            std::cout << "Coeff. of stickiness:\t" << stickiness << "\n\n";
            std::cout << "Coeff. of air resistance:\t" << airResist<< "\n";
            std::cout << "Wind sinks/sources strength:\t" << sinkSourceStrength<< "\n";
            std::cout << "Wind vortices strength:\t" << vortexStrength << "\n";
            std::cout << "Base radius of wind spheres.\t" << windBaseRadius << "\n";
            std::cout << "\n";

            rbState->print();
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
        }
    };

  //--------------------------------------------------------
  private:
    Solver solver;
    RigidBodyState rbState;
    SoftBodyState collMeshState;
    Mesh *mesh; 
    Mesh *rbMesh;
    int numParticles; 
    double gravMag, velMag, angVelMag; 
    double restitution, stickiness;
    double airResist, windBaseRadius; 
    double sinkSourceStrength, vortexStrength;

}; 	//End class declaration



//--------------------------------------------------------
pba::PbaThing makeWindSystem()
{
    return PbaThing( new pba::WindSystem() );
}


}  //End namespace pba



#endif

