#include "Vector.h"
#include "DataStructs.h"

class WindForces
{
  public:
    WindForces();
    ~WindForces();

    pba::Vector getWindVel(pba::Vector pos);
    void setSourceSinkStrength(double s);
    void setVortexStrength(double s);
    void setRadius(double r);

  private:
    int fieldW, fieldH, fieldD;
    double edgeLength, sinkSourceStrength; 
    double vortexStrength, baseRadius;
    pba::Vector bottomLeft, topRight;
    pba::Vector *field; 
   
    pba::Vector getVelFromSinkVortex(SinkVortex sink, pba::Vector coord, bool isSink);
    void genWindField();
    void printWindField();

};  //End class

