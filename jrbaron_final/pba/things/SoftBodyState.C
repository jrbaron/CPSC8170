#include "SoftBodyState.h"
#include "LinearAlgebra.h"
#include <iostream>

namespace pba{

//-------------------------------------
SoftBodyStateData::SoftBodyStateData(const std::string &nam) :
    DynamicalStateData(nam)
{ }

//-------------------------------------
SoftBodyStateData::~SoftBodyStateData()
{ 
    softEdges.clear();
    softTris.clear();
}

//-------------------------------------
std::vector<SoftEdge> SoftBodyStateData::getSoftEdges()
    { return softEdges; }

//-------------------------------------
void SoftBodyStateData::addSoftEdge(SoftEdge pair)
    { softEdges.push_back(pair); }

//-------------------------------------
void SoftBodyStateData::addSoftTri(SoftTriangle tri)
    { softTris.push_back(tri); }

//-------------------------------------
Vector SoftBodyStateData::calcStrutForce(int i, double springStrength, double frictionStrength)
{ 
    int cnt, j, p;
    Vector spring, friction, xij, xij_norm, xji_norm;
    SoftEdge pair;

    cnt = softEdges.size();
    for(p=0; p<cnt; p++)
    {
        pair = softEdges[p];
        if (pair.i == i)
        {
            j = pair.j;
            xij = pos(j)-pos(i);
            xij_norm = xij;
            xij_norm.normalize();
            xji_norm = pos(i)-pos(j);
            xji_norm.normalize();

          //The spring force causes attraction (if far away) and repulsion (if too close) of the points.
            spring += (springStrength * ((xij.magnitude() - pair.length) * xij_norm));

          //The friction force causes a loss of energy, dampening jiggling from the spring force.
            friction += (frictionStrength * xij_norm * (xij_norm * (vel(j)-vel(i))) );
        //    friction -= (frictionStrength * xij_norm * (xji_norm * (vel(j)-vel(i))) );
            
        }
    }

    return spring+friction;
}

//-------------------------------------
Vector SoftBodyStateData::calcTriAreaForce(int i, double areaStr, double frictionStr)
{ 
    Vector x0, x1, x2, d0, d1, d2; 
    Vector f0, f1, f2, v0, v1, v2, totalFrc;
    SoftTriangle tri;
    int cnt, t;
    float area;

    cnt = softTris.size();
    for(t=0; t<cnt; t++)
    {
        tri = softTris[t];
        if ( (tri.i == i) || (tri.j == i) || (tri.k == i) )
        {  
            x0 = pos(tri.i);    x1 = pos(tri.j);    x2 = pos(tri.k);
            v0 = vel(tri.i);    v1 = vel(tri.j);    v2 = vel(tri.k);

            area = 0.5 * (x1-x0).magnitude() * (x2-x0).magnitude();

            d0 = (x0 - 0.5*(x1+x2));
            d0.normalize();
            d1 = (x1 - 0.5*(x0+x2));
            d1.normalize();
            d2 = (x2 - 0.5*(x0+x1));
            d2.normalize();

            f0 = -areaStr * (area - tri.area) * d0;
            f1 = -areaStr * (area - tri.area) * d1;
            f2 = -areaStr * (area - tri.area) * d2;

            v0 = v0 - 0.5*(v1+v2);
            v1 = v1 - 0.5*(v0+v2);
            v2 = v2 - 0.5*(v0+v2);

            f0 = f0 + (-frictionStr * d0 * (d0 * v0));
            f1 = f1 + (-frictionStr * d1 * (d1 * v1));
            f2 = f2 + (-frictionStr * d2 * (d2 * v2));

            if (i == tri.i)
                { totalFrc += f0; }
            else if (i == tri.j)
                { totalFrc += f1; }
            else 
                { totalFrc += f2; }

        }  
    }

    return totalFrc;
}

//-------------------------------------


} //End namespace

//SoftBodyState type defined in header file.
pba::SoftBodyState pba::createSoftBodyState( const std::string& nam )
{
   return pba::SoftBodyState( new pba::SoftBodyStateData(nam) );
}

