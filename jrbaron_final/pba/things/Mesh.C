#include "Mesh.h"

Mesh::Mesh(char *fname, int edges) : 
    filename(fname), 
    numEdges(edges),
    numFaces(0), 
    numVerts(0), 
    numNormals(0), 
    numTexCoords(0),
    vertices(NULL), 
    finalVertices(NULL), 
    finalNormals(NULL), 
    finalTexCoords(NULL),
    triangles(NULL),
    pbaVectors(NULL),
    softEdges(),
    softTris(),
    isPlane(false),
    kdTreeRoot(NULL)
{
    printf("START making Mesh from OBJ file.\n");
    parseObj();
    makeTrisForObjMesh();
    makeKDTree();
    printKDTree();
    printf("FINISHED making Mesh from OBJ file.\n");
}

//-------------------------------------------
//width and depth are vertex (not face) counts.  Generate a plane along XZ of size width*depth.
//edges = 3 (tris) or 4 (quads); quad option for physics simulation purposes only, not for rendering.
Mesh::Mesh(int width, int depth, int holeW, int holeD, double edgeLength, int edges, double y) :     
    filename(NULL), 
    numEdges(edges),
    numFaces(0), 
    numVerts(0), 
    numNormals(0), 
    numTexCoords(0),
    vertices(NULL), 
    finalVertices(NULL), 
    finalNormals(NULL), 
    finalTexCoords(NULL),
    triangles(NULL),
    pbaVectors(NULL),
    softEdges(),
    softTris(),
    isPlane(true)
{
    printf("STARTING constructing a plane mesh with width=%d, depth=%d,\n\tedge length=%f, y-coord=%f\n",
        width, depth, edgeLength, y);

    int i, j, vertID, triID(0), halfW, halfD; 
    int startHoleW, startHoleD, endHoleW, endHoleD;
    double x, z, triArea;
    bool alongEdge, alongHole;

    numVerts = width*depth;
    numFaces = (width)*(depth)*2;  //2 triangles per quad.
    numNormals = numFaces;
    finalVertices = (GLfloat *) malloc(3 * numFaces * 3 * sizeof(GLfloat)); //3 verts per face (when displaying) * components (3) per item
    pbaVectors = (pba::Vector*) malloc(numVerts * sizeof(pba::Vector));
    triangles = (struct Tri*) malloc(numFaces * sizeof(struct Tri));

    triArea = edgeLength*edgeLength*0.5;  //A = 1/2 b*h = 1/2 len*len 
    halfW = width/2;
    halfD = depth/2;
    startHoleW = (width-holeW)/2;
    startHoleD = (depth-holeD)/2;
    endHoleW = startHoleW + holeW;
    endHoleD = startHoleD + holeD;

printf("HoleW: start=%d, end=%d\n", startHoleW, endHoleW);
printf("HoleD: start=%d, end=%d\n", startHoleD, endHoleD);

    kdTreeRoot = new KDTreeNode;
    kdTreeRoot->aabb.lowerLeft.set(-halfW*edgeLength,  y-(0.25*edgeLength),  -halfD*edgeLength); 
    kdTreeRoot->aabb.upperRight.set((width-halfW)*edgeLength,  y+(0.25*edgeLength),  (depth-halfD)*edgeLength);

    for(i=0; i<width; i++)  //-1 because +1 to indices is done.
    {
        for(j=0; j<depth; j++)
        {
            if ( (i <= startHoleW) || (i >= endHoleW) || (j <= startHoleD) || (j >= endHoleD))   //If some coord makes it not within hole
            {
                //Populate pbaVectors, softEdges, and finalVertices arrays.
                    vertID = i*depth + j;
                    x = edgeLength*(i-halfW);
                    z = edgeLength*(j-halfD);
                    pbaVectors[vertID].set(x, y, z);

                //Max of 4 connections per vertex. Min of 2 if at corners.
                    if ((i != 0) && (i != endHoleW))   //Don't want to move back by j (the -depth) when there won't be a vertex.
                        { softEdges.push_back( (SoftEdge){vertID, vertID-depth, edgeLength}); }

                    if ((j != 0) && (j != endHoleD))   //Likewise don't move back by i (the -1).
                        { softEdges.push_back( (SoftEdge){vertID, vertID-1, edgeLength}); }

                    if ((i != width-1) && (i != startHoleW))   //Don't move forward by j when there won't be a vertex. 
                        { softEdges.push_back( (SoftEdge){vertID, vertID+depth, edgeLength}); }

                    if ((j != depth-1) && (j != startHoleD))   //And don't move forward by i.
                        { softEdges.push_back( (SoftEdge){vertID, vertID+1, edgeLength}); }

                  //One extra connection along the diagonal.
                    if (numEdges == 3)  
                    {
                        alongEdge = (i == width-1) || (j == depth-1); 
                        alongHole = ( ((i == startHoleW) && (j < endHoleD) && (j >= startHoleD)) 
                                    || ((j == startHoleD) && (i < endHoleW) && (i >= startHoleW)) );
                        if ( !alongEdge && !alongHole)
                            { softEdges.push_back( (SoftEdge){vertID, vertID+depth+1, edgeLength}); }
                    }

                    if ((i != width-1) && (j != depth-1))  //If not on one half of the border
                    { 
                        triangles[triID].v0.set(x, y, z);
                        triangles[triID].v1.set(x+edgeLength, y, z);
                        triangles[triID].v2.set(x, y, z+edgeLength);

                        triangles[triID+1].v0.set(x+edgeLength, y, z+edgeLength);
                        triangles[triID+1].v1.set(x, y, z+edgeLength);
                        triangles[triID+1].v2.set(x+edgeLength, y, z);

                        softTris.push_back( (SoftTriangle){vertID, vertID+depth, vertID+1, triArea} );
                        softTris.push_back( (SoftTriangle){vertID+1+depth, vertID+1, vertID+depth, triArea} );

                        triID += 2;
                    } 

            } //End if not hole width nor depth 

        } //End for j
    } //End for i

    makeKDTree();
    printKDTree();

    printf("FINISHED constructing a plane mesh.\n");

}

//-------------------------------------------
//free() per malloc() and delete per new.
Mesh::~Mesh() 
{
    if (vertices != NULL)
        { free(vertices); }

    if (finalVertices != NULL)
        { free(finalVertices); }

    if (finalNormals != NULL)
        { free(finalNormals); }

    if (finalTexCoords != NULL)
        { free(finalTexCoords); }

    if (triangles != NULL)
        { free(triangles); }

    if (pbaVectors != NULL)
        { free(pbaVectors); }

 //Traverse KD-Tree to delete each node.
    if (kdTreeRoot != NULL)    
    {
        printf("Deleting KD Tree.\n");
        KDTreeNode *node, *temp;
          
        node = kdTreeRoot;
        while (node->half0 != NULL)
        {
            temp = node->half0;
            delete node;
            node = temp;
        }

        node = kdTreeRoot;
        while (node->half1 != NULL)
        {
            temp = node->half1;
            delete node;
            node = temp;
        }

        delete kdTreeRoot;
    }

} 

//-------------------------------------------
void Mesh::makeTrisForObjMesh()
{
    printf("Mesh::makeTrisForObjMesh()\n"); 
    int i, v;
    triangles = (struct Tri*) malloc(numFaces * sizeof(struct Tri));
    for (i=0; i<numFaces; i++)
    {
        v = i*numEdges*3;   
        triangles[i].v0.set(finalVertices[v], finalVertices[v+1], finalVertices[v+2] );
        triangles[i].v1.set(finalVertices[v+3], finalVertices[v+4], finalVertices[v+5] );  
        triangles[i].v2.set(finalVertices[v+6], finalVertices[v+7], finalVertices[v+8] );  
    }
}

//-------------------------------------------
//Returns array of Tri structs for the PBA classes to use.
Tri* Mesh::getTris()
{ 
    return triangles; 
}

//-------------------------------------------
KDTreeNode* Mesh::getKDTree()
    { return kdTreeRoot; }

//-------------------------------------------
KDTreeNode* Mesh::getKDNodeOfPoint(pba::Vector pt)
{
    KDTreeNode *node, *finalNode=NULL;  //In using finalNode, make sure to check to the furthest depth.

  //  printf("HALF0\n");
    node = kdTreeRoot;
    while (node->half0 != NULL)
    {
       // printf("\tLevel: %d\n", node->level);
       // printf("\tNum tris: %d\n", node->triList.size());
        if (isPointInAABB(pt, node->aabb))
        { 
            finalNode = node; 
        }    
        node = node->half0;
    }

  //  printf("HALF1\n");
    node = kdTreeRoot;
    while (node->half1 != NULL)
    {
       // printf("\tLevel: %d\n", node->level);
       // printf("\tNum tris: %d\n", node->triList.size());
        if (isPointInAABB(pt, node->aabb))
        { 
            finalNode = node; 
        }    
        node = node->half1;
    }
   
    return finalNode;
}

//-------------------------------------------
bool Mesh::isPointInAABB(pba::Vector pt, AABB aabb)
{
    if ( (pt.X() < aabb.lowerLeft.X()) || (pt.X() > aabb.upperRight.X()) )
        { return false; }
    if ( (pt.Y() < aabb.lowerLeft.Y()) || (pt.Y() > aabb.upperRight.Y()) )
        { return false; }
    if ( (pt.Z() < aabb.lowerLeft.Z()) || (pt.Z() > aabb.upperRight.Z()) )
        { return false; }
    return true;
}

//-------------------------------------------
bool Mesh::isTriInAABB(Tri tri, AABB aabb)
{
    return ( isPointInAABB(tri.v0, aabb) 
          && isPointInAABB(tri.v1, aabb)
          && isPointInAABB(tri.v2, aabb) );
}

//-------------------------------------------
pba::Vector* Mesh::getPbaVectors()
{
  int i, v;

  if (!isPlane)
  {
    pbaVectors = (pba::Vector*) malloc(numVerts * sizeof(pba::Vector));
    //Get info from the 'vertices' array. 'finalVertices'
    //  has repeated verts in order according to faces.
    for (i=0; i<numVerts; i++)
    {
      v = i*3;   
      pbaVectors[i].set( vertices[v], vertices[v+1], vertices[v+2] );
    }
  }

  return pbaVectors;
}

//-------------------------------------------
void Mesh::setStatesSoftConnections(pba::SoftBodyState &state)
{
    if (softEdges.size() != 0)
    { 
        int i; 
        for(i=0; i<softEdges.size(); i++)   //3 edges (pairs) per face
            { state->addSoftEdge(softEdges[i]); }
    }
    else
    {
        printf("Mesh::softEdges is empty.");
    }

    if (softTris.size() != 0)
    { 
        int i; 
        for(i=0; i<softTris.size(); i++)  
            { state->addSoftTri(softTris[i]); }
    }
    else
    {
        printf("Mesh::softTris is empty.");
    }
}

//-------------------------------------------
std::vector<SoftEdge> Mesh::getSoftEdges()
    { return softEdges; }

//-------------------------------------------
GLfloat* Mesh::getVertexArray()
    { return finalVertices; }

//-------------------------------------------
bool Mesh::getIsPlane()
    { return isPlane; }

//-------------------------------------------
int Mesh::getNumFaces()
    { return numFaces; }

//-------------------------------------------
int Mesh::getNumVertices()
    { return numVerts; }

//-------------------------------------------
void Mesh::splitKDTreeNode(KDTreeNode *node, char axis)
{
   int minTriCount = 5; 
   pba::Vector LL, UR;
   char newAxis;
   bool hasTri0, hasTri1;
   double len;

   if (node->triList.size() < minTriCount)
      { return; }
   else
   {
       node->half0 = new KDTreeNode;
       node->half1 = new KDTreeNode;

       node->half0->level = node->level + 1;
       node->half1->level = node->level + 1;

       LL = node->aabb.lowerLeft;
       UR = node->aabb.upperRight;

       if (axis == 'x')   //Cut X in half but keep Y and Z the same.
       { 
           node->half0->aabb.lowerLeft = LL; 
           node->half0->aabb.upperRight.set( UR.X()-0.5*len, UR.Y(), UR.Z());
           node->half1->aabb.lowerLeft.set( UR.X()-0.5*len, LL.Y(), LL.Z());
           node->half1->aabb.upperRight = UR; 
           newAxis = 'y';
       }

       else if (axis == 'y')
       { 
           len = (UR.Y()-LL.Y());
           node->half0->aabb.lowerLeft = LL; 
           node->half0->aabb.upperRight.set( UR.X(), UR.Y()-0.5*len, UR.Z());
           node->half1->aabb.lowerLeft.set( LL.X(), UR.Y()-0.5*len, LL.Z());
           node->half1->aabb.upperRight = UR; 
           newAxis = 'z';
       }

       else if (axis == 'z')
       { 
           len = (UR.Z()-LL.Z());
           node->half0->aabb.lowerLeft = LL; 
           node->half0->aabb.upperRight.set( UR.X(), UR.Y(), UR.Z()-0.5*len);
           node->half1->aabb.lowerLeft.set( LL.X(), LL.Y(), UR.Z()-0.5*len);
           node->half1->aabb.upperRight = UR; 
           newAxis = 'x';
       }


       for (auto tri : node->triList)
       {
           hasTri0 = isTriInAABB(tri, node->half0->aabb);
           hasTri1 = isTriInAABB(tri, node->half1->aabb);

           if (hasTri0)
               { node->half0->triList.push_back( tri ); }
           if (hasTri1)
               { node->half1->triList.push_back( tri ); }
       }

       splitKDTreeNode(node->half0, newAxis);
       splitKDTreeNode(node->half1, newAxis);
   }
}

//-------------------------------------------
void Mesh::makeKDTree()
{ 
   printf("Mesh::makeKDTree()\n");

   int i;
   kdTreeRoot->level = 0; 

   for (i=0; i<numFaces; i++)
       { kdTreeRoot->triList.push_back( triangles[i] ); }

   splitKDTreeNode(kdTreeRoot, 'x');
}

//-------------------------------------------
void Mesh::printKDTree()
{ 
    KDTreeNode *node;
    node = kdTreeRoot;

    printf("Traversing half0:\n");
    while (node->half0 != NULL)
    {
        printf("\tLevel: %d\n", node->level);
        printf("\tNum tris: %d\n", node->triList.size());
        node = node->half0;
    }

    node = kdTreeRoot;
    printf("Traversing half1:\n");
    while (node->half1 != NULL)
    {
        printf("\tLevel: %d\n", node->level);
        printf("\tNum tris: %d\n", node->triList.size());
        node = node->half1;
    }

} 

//-------------------------------------------
//Parses an OBJ file and updates global geometry arrays (vertices, normals, etc.).
void Mesh::parseObj()
{
    FILE *fptr;
    char *parse;
    int lineLength = 1024, i;
    int vertInd = 0, texCoordInd = 0, normalInd = 0, faceInd = 0;
    char buf[lineLength];
    GLuint *faceVerts, *faceNormals, *faceTexCoords;
    GLfloat *normals, *texCoords;
    GLfloat x, y, z, minX, minY, minZ, maxX, maxY, maxZ;


    fptr = fopen(filename, "r");    if (!fptr) printf("Could not load file %s as OBJ.\n", filename);
    while ( fgets(buf, lineLength, fptr) )          //First get all counts.
    {
            if (buf[0] == 'v')
            {
                    if (buf[1] == ' ')      numVerts++;
                    else if (buf[1] == 't') numTexCoords++;
                    else if (buf[1] == 'n') numNormals++;
            }
            else if (buf[0] == 'f') numFaces++;
    }
    rewind(fptr);                   //Return pointer to beginning of file.

    vertices = (GLfloat *) malloc(numVerts * 3 * sizeof(GLfloat));
    texCoords = (GLfloat *) malloc(numTexCoords * 2 * sizeof(GLfloat));
    normals = (GLfloat *) malloc(numNormals * 3 * sizeof(GLfloat));

    faceVerts = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));            //This saves just vertex indices. 
    faceTexCoords = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));        //Faces also have vt and vn info.       
    faceNormals = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));

    //printf("Num verts: %d \nNum edges per face: %d \nNum faces: %d \nNum texcoords %d \nNum normals %d \n",
    //        numVerts, numEdges, numFaces, numTexCoords, numNormals);

    while ( fgets(buf, lineLength, fptr) )
    {
            if (buf[0] == 'v')
            {
                    if (buf[1] == ' ')
                    {
                            parse = strtok(buf, " \t");     //Strip 'v'.
                            parse = strtok(NULL, " \t");    //x-coord
                            //vertices[vertInd] = atof(parse);
                            x = atof(parse);
                            vertices[vertInd] = x;
                            if (vertInd == 0)
                                { minX = x; maxX = x; }
                            else
                            {
                                if (x < minX) {minX = x;}
                                if (x > maxX) {maxX = x;}
                            }

                            parse = strtok(NULL, " \t");    //y-coord
                            //vertices[vertInd+1] = atof(parse);
                            y = atof(parse);
                            vertices[vertInd+1] = y;
                            if (vertInd == 0)
                                { minY = y; maxY = y; }
                            else
                            {
                                if (y < minY) {minY = y;}
                                if (y > maxY) {maxY = y;}
                            }

                            parse = strtok(NULL, " \t\n");  //z-coord
                            //vertices[vertInd+2] = atof(parse);
                            z = atof(parse);
                            vertices[vertInd+2] = z;
                            if (vertInd == 0)
                                { minZ = z; maxZ = z; }
                            else
                            {
                                if (z < minZ) {minZ = z;}
                                if (z > maxZ) {maxZ = z;}
                            }

                            vertInd += 3;                   //3 coords per vertex
                    }
                    else if (buf[1] == 't')
                    {
                            parse = strtok(buf, " \t");     //Strip 'vt'.
                            parse = strtok(NULL, " \t");    //s-coord
                            texCoords[texCoordInd] = atof(parse);
                            parse = strtok(NULL, " \t\n");  //t-coord
                            texCoords[texCoordInd+1] = atof(parse);
                            texCoordInd += 2;               //2 coords per tex coord        
                    }
                    else if (buf[1] == 'n')
                    {
                            parse = strtok(buf, " \t");     //Strip 'vn'.
                            parse = strtok(NULL, " \t");    //x-coord
                            normals[normalInd] = atof(parse);
                            parse = strtok(NULL, " \t");    //y-coord
                            normals[normalInd+1] = atof(parse);
                            parse = strtok(NULL, " \t\n");  //z-coord
                            normals[normalInd+2] = atof(parse);
                            normalInd += 3;                 //3 coords per normal 
                    }
            }

            else if (buf[0] == 'f')
            {
                parse = strtok(buf, " \t");             //Strip 'f'.
                for (i=0; i<numEdges; i++)
                {
                    if ( (numTexCoords != 0) && (numNormals != 0) )
                    {
                        parse = strtok(NULL, "/");                      //Vertex index of i-th face vertex
                        faceVerts[faceInd+i] = atoi(parse)-1;           //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
                        parse = strtok(NULL, "/");                      //Tex coords index of i-th face vertex
                           
                        faceTexCoords[faceInd+i] = atoi(parse)-1;        //-1!
                        if (i==numEdges)
                            { parse = strtok(NULL, " \t\n"); }              //Normal index of LAST face vertex (at end of line)
                        else
                            { parse = strtok(NULL, " \t"); }                //Normal index of i-th face vertex

                        faceNormals[faceInd+i] = atoi(parse) -1; 
                    }
       
                    else
                    {
                        if (i==numEdges)
                            { parse = strtok(NULL, " \t\n"); }              //Vertex index of LAST face vertex (at end of line)
                        else
                            { parse = strtok(NULL, " \t"); }                //Vertex index of i-th face vertex
                        //printf("\tparse: %s\n", parse);
                        faceVerts[faceInd+i] = atoi(parse)-1;               //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.

                    }
                }
                faceInd += numEdges;

            }
    }

    fclose(fptr);


//Approach without GL element arrays. (DrawElements does not allow different indices for vertices and normals.)
    finalVertices = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat)); //4 verts per face * components (3 or 2) per item
    finalNormals = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat));
    finalTexCoords = (GLfloat *) malloc(numEdges * numFaces * 2 * sizeof(GLfloat));

    int fVert, indV, indN, indTex;
    for(fVert=0; fVert<numFaces*numEdges; fVert++)
    {
        indV = faceVerts[fVert];
        for (i=0; i<3; i++)
        {
            finalVertices[fVert*3+i] = vertices[indV*3+i];
        }

       if ( (numNormals != 0) && (numTexCoords != 0) )
       {
            indN = faceNormals[fVert];
            indTex = faceTexCoords[fVert];
            for (i=0; i<3; i++)
            {
                finalNormals[fVert*3+i] = normals[indN*3+i];
            }
            for (i=0; i<2; i++)
            {
                finalTexCoords[fVert*2+i] = texCoords[indTex*2+i];
            }
        }
       
    }

  //Make sure a KD Tree won't be in 2D.
    if (maxX-minX == 0.0) { minX=minX-0.5; maxX=maxX+0.5; }
    if (maxY-minY == 0.0) { minY=minY-0.5; maxY=maxY+0.5; }
    if (maxZ-minZ == 0.0) { minZ=minZ-0.5; maxZ=maxZ+0.5; }
    printf("OBJ min/max X Y Z: \n\tx:%f,%f\ty:%f,%f\tz:%f,%f\n", minX, maxX, minY, maxY, minZ, maxZ);

    kdTreeRoot = new KDTreeNode;
    kdTreeRoot->aabb.lowerLeft.set(minX, minY, minZ);
    kdTreeRoot->aabb.upperRight.set(maxX, maxY, maxZ);

    free(normals);
    free(texCoords);
    free(faceVerts);
    free(faceNormals);
    free(faceTexCoords);
}
