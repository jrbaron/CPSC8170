//-------------------------------------------------------
//
//  Solver.h
//  A base solver class from which all solvers are derived.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SOLVERTHING_H__
#define __SOLVERTHING_H__

#include "Mesh.h"
#include "RigidBodyState.h"
#include <iostream>

namespace pba
{

class SolverThing
{
  public:
    SolverThing( const std::string nam="SolverThing_NoName" );
    virtual ~SolverThing() {};
    virtual void solve(double dt, RigidBodyState &rbState, Mesh &mesh) {};
    virtual void setWindForcesStrengths(double s, double v) {}; 
    virtual void setWindRadius(double r) {}; 
    virtual void setGravMagnitude(double g) {}; 
    virtual void setAirResistRoughness(double r) {}; 
    virtual void setRestitution(double r) {}; 
    virtual void setStickiness(double s) {};

  protected:
    std::string name;

};  //End class


typedef std::shared_ptr<SolverThing> Solver;

}  //End namespace


#endif
