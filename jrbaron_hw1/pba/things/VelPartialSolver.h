//-------------------------------------------------------
//
//  VelPartialSolver.h
//  A partial solver class for velocity.
//     S(v)(dt) {x,v} = {x, (v + f(x,v)/m *dt)}   //Update vel by a timestep of accel.
//  Also implements downward gravity.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class VelPartialSolver : public SolverThing
{
  public:
    VelPartialSolver() :
        SolverThing("VelPartialSolver")
    {

    };

    ~VelPartialSolver() {};

  //--------------------------------------
    void setGravMagnitude(double g)
    {
        gravMagnitude = g;
    };

  //--------------------------------------
    void solve(double dt, DynamicalState &state, Mesh &mesh)
    {
        Vector x, v, frc;
        Vector g(0, gravMagnitude*-9.8, 0); 	
        int i, count;
        double m;
        
        count = state->nb(); 
        for (i=0; i<count; i++)
        {
            x = state->pos(i);
            v = state->vel(i);
            m = state->mass(i);
            frc = g*m;

            state->set_vel( i, v + dt*frc/m );
        }
   
    };

  //--------------------------------------
  private:
  

};  //End class


pba::Solver createVelPartialSolver()
{
    return Solver( new pba::VelPartialSolver() );
}

}  //End namespace
