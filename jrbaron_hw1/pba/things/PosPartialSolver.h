//-------------------------------------------------------
//
//  PosPartialSolver.h
//  A partial solver class for position. Determines and handles 
//      collision based on Mesh information passed in. 
//    S(x)(dt) {x,v} = {(x + dt*v), v}       //Update pos by a timestep of vel.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __POSPARTIAL_H__
#define __POSPARTIAL_H__

#include <cmath>
#include <vector>
#include <algorithm>  //For std:max_element()

namespace pba
{

class PosPartialSolver : public SolverThing
{
  public:
    PosPartialSolver() :
        SolverThing("PosPartialSolver")
    {

    };

    ~PosPartialSolver() {};

  //--------------------------------------
  //Inherited function. Should be called by other solvers 
  //    when getting information down to this level.
  void setRestitution(double r)
     { restitution = r; } 

  //--------------------------------------
  //Inherited function
  void setStickiness(double s)
     { stickiness = s; } 

  //--------------------------------------
  //x: Particle position (if getting a timestep to trace back 
  //    to intersection, this is the *next* position.)
  //v: Current velocity of the particle.
  //Returns a timestep to walk back to the time of intersection between a particle and a plane.
  //    If no collision, return -1. (Timestep if returned should be in [0,1].)
    double detectCollWithPlane( Vector x, Vector v, double dt, Tri tri )
    {
        double q0, q1, dt_i, eps(0.00001);
        Vector np, edge0, edge1, xp; 

        xp = tri.v0;    //Any of the three triangle verts are a point on the plane.
        edge0 = tri.v1 - tri.v0;
        edge1 = tri.v2 - tri.v0;
      //^ for cross product
        np = edge0 ^ edge1;
        np.normalize(); 

      // * for dot product
        q0 = (x - xp) * np;     //Plane normal projected by distance between curr pos and pos on plane.
        q1 = (x - (v*dt) - xp) * np;

      //No collision if q0 == 0 (using an epsilon to consider 0 as [-eps,eps]).
        if ( (-eps <= q0) && (q0 <= eps) )
            { return -1; }

      //No collision here also.
        if ( q0*q1 > 0 )
            { return -1; }

      //Otherwise, so far there's a collision.
        dt_i = ((x-xp)*np) / (v*np);

      //Sanity check in case values had gotten too close to zero:
        if (dt*dt_i < 0)
            { return -1; }

        if (dt_i > dt)
            { return -1; }

        if ( (dt-dt_i)/dt < eps )    //If collision happened really soon when x is "on" the plane.
            { return -1; }

        return dt_i;

    };

  //--------------------------------------
  //Called after detectCollWithPlane() if there was a collision.
  //Now, see if that collision was with the triangle of interest.
    bool detectCollWithTri( Vector xi, Tri tri )
    {
        Vector edge0, edge1;
        double a, b;           //Barycentric coordinates
        
        edge1 = tri.v2 - tri.v0;
        edge0 = tri.v1 - tri.v0;
        
       //^ for cross product, * for dot product
       //No collision if barycentric coords and their sums are not within [0,1] range.
        a = (edge1^edge0) * (edge1 ^ (xi - tri.v0));
        a = a / ( pow((edge1^edge0).magnitude(), 2) );
        if ( (a<0) || (a>1) )
            { return false; } 

        b = (edge0^edge1) * (edge0 ^ (xi - tri.v0));
        b = b / ( pow((edge0^edge1).magnitude(), 2) );
        if ( (a<0) || (a>1) )
            { return false; } 

        if ( (a+b < 0) || (a+b > 1) )
            { return false; } 

       return true;
    };

  //--------------------------------------
  //x: Current particle position
  //xn: Next timestep particle position.
  //v: Current velocity of the particle.
    double detectCollision( Vector x, Vector xn, Vector v, double dt, Tri tri )
    {
        Vector xi;
        double dt_i;
        bool collide;
        dt_i = detectCollWithPlane(xn, v, dt, tri);   //xi is where the particle collides with a plane made out of the triangle.

        if (dt_i != -1)                 //Now see if this position is actually within the triangle. 
        {
            xi = x - v*dt_i;
            collide = detectCollWithTri(xi, tri);

            if (collide)
                { return dt_i; }
            else
                { return -1; }
        }
        else
        { return -1; }

    };

  //--------------------------------------
  //For each particle of the system, determine its position at the next timestep.
  //    Use this position and compare with each triangle of the Mesh to detect and 
  //    handle a collision if necessary.
    void solve(double dt, DynamicalState &state, Mesh &mesh)
    {
        Vector x, v, xi, nextX, vr, v_perp, np, edge0, edge1;
        Color col, newColor;
        Tri *tris, hitTri;
        int p, tr, i, numParticles, numTris;
        double dt_i, max_dt_i;
        std::vector<double> dt_i_list;
        std::vector<Tri> hitTri_list;
        KDTreeNode kdTreeRoot;
        
        numParticles = state->nb(); 
        numTris = mesh.getNumFaces(); 
        tris = mesh.getTris();
        kdTreeRoot = mesh.getKDTree();

        for (p=0; p<numParticles; p++)
        {
            x = state->pos(p);
            v = state->vel(p);
            col = state->ci(p);
            nextX = x+dt*v;

            for (tr=0; tr<numTris; tr++)
            {
                dt_i = detectCollision(x, nextX, v, dt, tris[tr]);
                if (dt_i != -1)
                { 
                    //printf("p=%d, tr=%d, dt_i: %f\n", p, tr, dt_i);
                    dt_i_list.push_back(dt_i); 
                    hitTri_list.push_back( tris[tr] );
                }
            }


         //Handle collision for largest dt_i (first that occurred, closest to x). 
            if (dt_i_list.size() > 0)
            { 
                max_dt_i = dt_i_list[0];
                hitTri = hitTri_list[0];
                i = 0;
                for (double hit_dt : dt_i_list)
                {
                    if (hit_dt > max_dt_i)
                    {
                        max_dt_i = hit_dt;
                        hitTri = hitTri_list[i];
                    } 
                    i++;
                }

                edge0 = hitTri.v1 - hitTri.v0;
                edge1 = hitTri.v2 - hitTri.v0;
                np = edge0 ^ edge1;
                np.normalize();
                v_perp = v - np*(np*v);

            //Compute reflected velocity as inelastic reflection.
                vr = stickiness*v_perp - restitution*np*(np*v);

                xi = nextX - max_dt_i*v;       //Pos at intersection is tracing back from the next time step pos by the intersection timestep found.
                nextX = xi + max_dt_i*vr;      //Change from intersection pos in dir of reflection

              //Increase blue with each collision. Once blue too high, turn white.
                if (col.blue() > 1)
                {
                    newColor.set( col.red()+0.1, col.green()+0.1, 1.0, 0 ); 
                }
                else 
                {
                    newColor.set( col.red(), col.green(), col.blue()+0.1, 0 ); 
                }

                state->set_pos(p, nextX);
                state->set_vel(p, vr);
                state->set_ci(p, newColor);
            }

            else
            {
                state->set_pos(p, nextX);
            }

            dt_i_list.clear();
        }
   
    };

  //--------------------------------------
  private:


};  //End class


pba::Solver createPosPartialSolver()
{
    return Solver( new pba::PosPartialSolver() );
}


}  //End namespace

#endif
