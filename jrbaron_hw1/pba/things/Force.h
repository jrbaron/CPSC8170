//-------------------------------------------------------
//
//  Force.h
//  A base force class from which all forces are derived.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __FORCETHING_H__
#define __FORCETHING_H__

#include <iostream>
#include <memory>    //Sometimes needed for Makefile to see the shared_ptr as a type?

namespace pba
{

class ForceThing
{
  public:
    ForceThing( const std::string name="ForceThing_NoName" );
    virtual ~ForceThing() {};

  private:
    

};  //End class


typedef std::shared_ptr<ForceThing> Force;

}  //End namespace


#endif
