//-------------------------------------------------------
//
//  Mesh.h
//  A mesh class to parse an OBJ file and maintain its data.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>    //malloc(), calloc(), rand()
#include <fcntl.h>     //open(), read(), write(), close()
#include <string.h>    //For atoi() with fgets()
#include <vector>

//For the GLfloat and GLuint types
#ifdef __APPLE__
  #include <OpenGL/gl.h>  
#else
  #include <GL/gl.h>  
#endif

#include "Vector.h"


#ifndef __MESH_H__
#define __MESH_H__


struct Tri
{ pba::Vector v0, v1, v2; };

struct AABB
{ pba::Vector lowerLeft, upperRight; };

struct KDTreeNode
{
    int level;
    AABB aabb;
    std::vector<Tri> triList;
    KDTreeNode *half0, *half1;
};

class Mesh
{
  public:
    Mesh(char *fname, int edges) : 
        filename(fname), 
        numEdges(edges),
        numFaces(0), 
        numVerts(0), 
        numNormals(0), 
        numTexCoords(0),
        finalVertices(NULL), 
        finalNormals(NULL), 
        finalTexCoords(NULL),
        triangles(NULL),
        kdTreeRoot()
    {
        parseObj();
        makeTris();
        makeKDTree();
    };

  //-------------------------------------------
    ~Mesh() 
    {
        if (finalVertices != NULL)
            { free(finalVertices); }

        if (finalNormals != NULL)
            { free(finalNormals); }

        if (finalTexCoords != NULL)
            { free(finalTexCoords); }

        if (triangles != NULL)
            { free(triangles); }

        //TODO: Clean up allocations from KDTree. Traverse it.

    };

  //-------------------------------------------
  //Returns array of Tri structs for the PBA classes to use.
    Tri* getTris()
    { 
        return triangles; 
    };

  //-------------------------------------------
  //Return a reference.
    KDTreeNode& getKDTree()
    { 
        return kdTreeRoot;
    };

  //-------------------------------------------
  //Returns array of Tri structs for the PBA classes to use.
    GLfloat* getVertexArray()
    { 
        return finalVertices; 
    };

  //-------------------------------------------
    int getNumFaces()
    {
        return numFaces;
    };

  //------------------------------------------------------------------
  private:
  //Parses an OBJ file and updates global geometry arrays (vertices, normals, etc.).
    void parseObj()
    {
        FILE *fptr;
        char *parse;
        int lineLength = 1024, i;
        int vertInd = 0, texCoordInd = 0, normalInd = 0, faceInd = 0;
        char buf[lineLength];
        GLfloat *vertices, *normals, *texCoords;
        GLfloat x, y, z, minX, minY, minZ, maxX, maxY, maxZ;
        GLuint *faceVerts, *faceNormals, *faceTexCoords;

        fptr = fopen(filename, "r");    if (!fptr) printf("Could not load file %s as OBJ.\n", filename);
        while ( fgets(buf, lineLength, fptr) )          //First get all counts.
        {
                if (buf[0] == 'v')
                {
                        if (buf[1] == ' ')      numVerts++;
                        else if (buf[1] == 't') numTexCoords++;
                        else if (buf[1] == 'n') numNormals++;
                }
                else if (buf[0] == 'f') numFaces++;
        }
        rewind(fptr);                   //Return pointer to beginning of file.

        vertices = (GLfloat *) malloc(numVerts * 3 * sizeof(GLfloat));
        texCoords = (GLfloat *) malloc(numTexCoords * 2 * sizeof(GLfloat));
        normals = (GLfloat *) malloc(numNormals * 3 * sizeof(GLfloat));

        faceVerts = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));            //This saves just vertex indices. 
        faceTexCoords = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));        //Faces also have vt and vn info.       
        faceNormals = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));

        //printf("Num verts: %d \nNum edges per face: %d \nNum faces: %d \nNum texcoords %d \nNum normals %d \n",
        //        numVerts, numEdges, numFaces, numTexCoords, numNormals);

        while ( fgets(buf, lineLength, fptr) )
        {
                if (buf[0] == 'v')
                {
                        if (buf[1] == ' ')
                        {
                                parse = strtok(buf, " \t");     //Strip 'v'.
                                parse = strtok(NULL, " \t");    //x-coord
                                x = atof(parse);
                                vertices[vertInd] = x;
                                if (vertInd == 0)
                                    { minX = x; maxX = x; }
                                else
                                {
                                    if (x < minX) {minX = x;}
                                    if (x > maxX) {maxX = x;}
                                }

                                parse = strtok(NULL, " \t");    //y-coord
                                y = atof(parse);
                                vertices[vertInd+1] = y;
                                if (vertInd == 0)
                                    { minY = y; maxY = y; }
                                else
                                {
                                    if (y < minY) {minY = y;}
                                    if (y > maxY) {maxY = y;}
                                }

                                parse = strtok(NULL, " \t\n");  //z-coord
                                z = atof(parse);
                                vertices[vertInd+2] = z;
                                if (vertInd == 0)
                                    { minZ = z; maxZ = z; }
                                else
                                {
                                    if (z < minZ) {minZ = z;}
                                    if (z > maxZ) {maxZ = z;}
                                }

                                vertInd += 3;                   //3 coords per vertex
                        }
                        else if (buf[1] == 't')
                        {
                                parse = strtok(buf, " \t");     //Strip 'vt'.
                                parse = strtok(NULL, " \t");    //s-coord
                                texCoords[texCoordInd] = atof(parse);
                                parse = strtok(NULL, " \t\n");  //t-coord
                                texCoords[texCoordInd+1] = atof(parse);
                                texCoordInd += 2;               //2 coords per tex coord        
                        }
                        else if (buf[1] == 'n')
                        {
                                parse = strtok(buf, " \t");     //Strip 'vn'.
                                parse = strtok(NULL, " \t");    //x-coord
                                normals[normalInd] = atof(parse);
                                parse = strtok(NULL, " \t");    //y-coord
                                normals[normalInd+1] = atof(parse);
                                parse = strtok(NULL, " \t\n");  //z-coord
                                normals[normalInd+2] = atof(parse);
                                normalInd += 3;                 //3 coords per normal 
                        }
                }

                else if (buf[0] == 'f')
                {
                    parse = strtok(buf, " \t");             //Strip 'f'.
                    for (i=0; i<numEdges; i++)
                    {
                        if ( (numTexCoords != 0) && (numNormals != 0) )
                        {
                            parse = strtok(NULL, "/");                      //Vertex index of i-th face vertex
                            faceVerts[faceInd+i] = atoi(parse)-1;           //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
                            parse = strtok(NULL, "/");                      //Tex coords index of i-th face vertex
                               
                            faceTexCoords[faceInd+i] = atoi(parse)-1;        //-1!
                            if (i==numEdges)
                                { parse = strtok(NULL, " \t\n"); }              //Normal index of LAST face vertex (at end of line)
                            else
                                { parse = strtok(NULL, " \t"); }                //Normal index of i-th face vertex

                            faceNormals[faceInd+i] = atoi(parse) -1; 
                        }
           
                        else
                        {
                            if (i==numEdges)
                                { parse = strtok(NULL, " \t\n"); }              //Vertex index of LAST face vertex (at end of line)
                            else
                                { parse = strtok(NULL, " \t"); }                //Vertex index of i-th face vertex
                            //printf("\tparse: %s\n", parse);
                            faceVerts[faceInd+i] = atoi(parse)-1;               //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
 
                        }
                    }
                    faceInd += numEdges;

                }
        }

        fclose(fptr);


   //Approach without GL element arrays. (DrawElements does not allow different indices for vertices and normals.)
        finalVertices = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat)); //4 verts per face * components (3 or 2) per item
        finalNormals = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat));
        finalTexCoords = (GLfloat *) malloc(numEdges * numFaces * 2 * sizeof(GLfloat));

        int fVert, indV, indN, indTex;
        for(fVert=0; fVert<numFaces*numEdges; fVert++)
        {
            indV = faceVerts[fVert];
            for (i=0; i<3; i++)
            {
                finalVertices[fVert*3+i] = vertices[indV*3+i];
            }

           if ( (numNormals != 0) && (numTexCoords != 0) )
           {
                indN = faceNormals[fVert];
                indTex = faceTexCoords[fVert];
                for (i=0; i<3; i++)
                {
                    finalNormals[fVert*3+i] = normals[indN*3+i];
                }
                for (i=0; i<2; i++)
                {
                    finalTexCoords[fVert*2+i] = texCoords[indTex*2+i];
                }
            }
           
        }

        kdTreeRoot.aabb.lowerLeft.set(minX, minY, minZ);
        kdTreeRoot.aabb.upperRight.set(maxX, maxY, maxZ);

        free(vertices);
        free(normals);
        free(texCoords);
        free(faceVerts);
        free(faceNormals);
        free(faceTexCoords);
    }

  //-------------------------------------------
    Tri* makeTris()
    { 
        if (numEdges != 3)
            { printf("Mesh::getTris(): ERROR: numEdges not set to 3 for Mesh instance.\n"); }

        Tri *triangles;
        int i, v;
        triangles = (struct Tri*) malloc(numFaces * sizeof(struct Tri));
        for (i=0; i<numFaces; i++)
        {
            v = i*numEdges*3;   
            triangles[i].v0.set(finalVertices[v], finalVertices[v+1], finalVertices[v+2] );
            triangles[i].v1.set(finalVertices[v+3], finalVertices[v+4], finalVertices[v+5] );  
            triangles[i].v2.set(finalVertices[v+6], finalVertices[v+7], finalVertices[v+8] );  
        }
        
    };

  //-------------------------------------------
    bool isTriInAABB(Tri tri, AABB aabb)
    {
        double Xs[3], Ys[3], Zs[3];

        Xs[0] = tri.v0.X(); Xs[1] = tri.v1.X(); Xs[2] = tri.v2.X();
        Ys[0] = tri.v0.Y(); Ys[1] = tri.v1.Y(); Ys[2] = tri.v2.Y();
        Zs[0] = tri.v0.Z(); Zs[1] = tri.v1.Z(); Zs[2] = tri.v2.Z();

        //Triangle to left of bounding box, not touching.
        if ( (Xs[0] < aabb.lowerLeft.X()) && (Xs[1] < aabb.lowerLeft.X()) && (Xs[2] < aabb.lowerLeft.X()) )
            { return false; }

        //Triangle to right of bounding box, not touching.
        if ( (Xs[0] > aabb.upperRight.X()) && (Xs[1] > aabb.upperRight.X()) && (Xs[2] > aabb.upperRight.X()) )
            { return false; }

        //Triangle below bounding box, not touching.
        if ( (Ys[0] < aabb.lowerLeft.Y()) && (Ys[1] < aabb.lowerLeft.Y()) && (Ys[2] < aabb.lowerLeft.Y()) )
            { return false; }

        //Triangle above bounding box, not touching.
        if ( (Ys[0] > aabb.upperRight.Y()) && (Ys[1] > aabb.upperRight.Y()) && (Ys[2] > aabb.upperRight.Y()) )
            { return false; }

        //Triangle in front of bounding box, not touching.
        if ( (Zs[0] < aabb.lowerLeft.Z()) && (Zs[1] < aabb.lowerLeft.Z()) && (Zs[2] < aabb.lowerLeft.Z()) )
            { return false; }

        //Triangle behind bounding box, not touching.
        if ( (Zs[0] > aabb.upperRight.Z()) && (Zs[1] > aabb.upperRight.Z()) && (Zs[2] > aabb.upperRight.Z()) )
            { return false; }

        return true;
    };

  //-------------------------------------------
    void splitKDTreeNode(KDTreeNode *node, char axis)
    {
       int minTriCount = 5;
       pba::Vector mid;
       char newAxis;
       bool hasTri0, hasTri1;
       double len;
printf("Level: %d\n", node->level);

       if (node->triList.size() < minTriCount)
           { return; }
       else
       {
           node->half0 = new KDTreeNode;
           node->half1 = new KDTreeNode;

           node->half0->level = node->level + 1;
           node->half1->level = node->level + 1;
printf("a\n");           
           mid = node->aabb.lowerLeft; 
           if (axis == 'x')   //Cut X in half but keep Y and Z the same.
           { 
               len = node->aabb.upperRight.X() - node->aabb.lowerLeft.X();
               mid.set(mid.X() + 0.5*len, mid.Y(), mid.Z()); 
               newAxis = 'y';
           }
           else if (axis == 'y')
           { 
               len = node->aabb.upperRight.Y() - node->aabb.lowerLeft.Y();
               mid.set(mid.X(), mid.Y() + 0.5*len, mid.Z()); 
               newAxis = 'z';
           }
           else if (axis == 'z')
           { 
               len = node->aabb.upperRight.Z() - node->aabb.lowerLeft.Z();
               mid.set(mid.X(), mid.Y(), mid.Z() + 0.5*len); 
               newAxis = 'x';
           }

printf("b\n");           
           node->half0->aabb.lowerLeft = node->aabb.lowerLeft;
           node->half0->aabb.upperRight = mid;
           node->half1->aabb.lowerLeft = mid;
           node->half1->aabb.upperRight = node->aabb.upperRight;

printf("c\n");           
           for (auto tri : node->triList)
           {
               hasTri0 = isTriInAABB(tri, node->half0->aabb);
               hasTri1 = isTriInAABB(tri, node->half1->aabb);
printf("\ttri\n");           

               if (hasTri0)
                   { node->half0->triList.push_back( tri ); }
               if (hasTri1)
                   { node->half1->triList.push_back( tri ); }
           }

           splitKDTreeNode(node->half0, newAxis);
           splitKDTreeNode(node->half1, newAxis);
       }
    };

  //-------------------------------------------
    void makeKDTree()
    { 
       kdTreeRoot.level = 0; 
       for (int i=0; i<numFaces; i++)
           kdTreeRoot.triList.push_back( triangles[i] );  

       splitKDTreeNode(&kdTreeRoot, 'x');
    };

  //-------------------------------------------
  // Private data
    char *filename;
    int numEdges, numFaces, numVerts, numNormals, numTexCoords;
    GLfloat *finalVertices, *finalNormals, *finalTexCoords;
    Tri* triangles;
    KDTreeNode kdTreeRoot;

  //-------------------------------------------
};  //End class

#endif 
