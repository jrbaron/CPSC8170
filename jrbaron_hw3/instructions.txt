CPSC 8170 ASSIGNMENT 3 - FALL 2017

Assemble a rigid body dynamics system in which the rigid body is initialized to points on the surface of specified geometry

- be able to read an obj file and initialize the particle positions at the vertices of the obj file.

- contain the RBD object within a cube that the RBD object collides with.
- The RBD object should start near (0,0,0) with an initial center of mass velocity and angular velocity.
- Keys should control the magnitude of the initial velocities.

The simulation system should use OpenGL to display the particles and geometry.

Make sure you code works with the geometry for the RBD object from the collection of models:
Models file
