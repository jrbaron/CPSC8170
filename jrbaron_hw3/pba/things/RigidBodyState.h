//-------------------------------------------------------
//
//  RigidBodyState.h
//  A subclass of DynamicalStateData that handles information on rigid body particles. 
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __RIGIDBODYSTATE_H__
#define __RIGIDBODYSTATE_H__

#include "DynamicalState.h"
#include "Vector.h"
#include "Matrix.h"
#include "LinearAlgebra.h"

namespace pba{

class RigidBodyStateData : public DynamicalStateData
{
  public:
    RigidBodyStateData(const std::string& nam = "RigidBodyDataNoName");
    ~RigidBodyStateData();

    void setCenterOfMassPos(Vector p);
    void setCenterOfMassVel(Vector v);
    void setAngularVel(Vector v);
    void setRotationMat(Matrix m);
    double getTotalMass();
    Vector getAngularVel();
    Matrix getRotationMat();
    Matrix getMomentOfInertia();
    Vector getCenterOfMassPos();
    Vector getCenterOfMassVel();
    Vector getVertPos(int i);
    Vector getRelVertPos(int i);

    void updateCenterOfMassPos(double dt);
    void updateCenterOfMassVel(Vector forces, double dt);
    void updateRotationMat(double dt);
    void print();
    void init(double velMag, double angVelMag);

  private:
    void updateInertia();

    double totalMass;        //M
    Matrix momentOfInertia;  //I
    Vector centerOfMassPos;  //x(cm)
    Vector centerOfMassVel;  //v(cm)
    Matrix rotationMat;      //R
    Vector angularVel;       //w (omega)

}; //End class


typedef std::shared_ptr<RigidBodyStateData> RigidBodyState;
RigidBodyState createRigidBodyState( const std::string& nam = "RigidBodyState" );


} //End namespace

#endif
