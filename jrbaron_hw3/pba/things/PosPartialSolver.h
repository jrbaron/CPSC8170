//-------------------------------------------------------
//
//  PosPartialSolver.h
//  A partial solver class for position. Determines and handles 
//      collision based on Mesh information passed in. 
//    S(x)(dt) {x,v} = {(x + dt*v), v}       //Update pos by a timestep of vel.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __POSPARTIAL_H__
#define __POSPARTIAL_H__

#include <cmath>
#include <vector>
#include <algorithm>  //For std:max_element()

namespace pba
{

//class PosPartialSolver : public SolverThing
class PosPartialSolver
{
  public:
    PosPartialSolver() :
      restitution(1.0),
      stickiness(1.0)
    { };

    ~PosPartialSolver() {};

  //--------------------------------------
  //Inherited function. Should be called by other solvers 
  //    when getting information down to this level.
  void setRestitution(double r)
     { restitution = r; } 

  //--------------------------------------
  //Inherited function
  void setStickiness(double s)
     { stickiness = s; } 

  //--------------------------------------
  //See if a collision was with the triangle of interest.
    bool detectCollWithTri( Vector xi, Tri tri )
    {
        Vector edge0, edge1, cross0, cross1;
        double a, b, mag0, mag1;           //Barycentric coordinates
        
        edge1 = tri.v2 - tri.v0;
        edge0 = tri.v1 - tri.v0;

        cross0 = edge1^edge0;
        cross1 = edge0^edge1;
        mag0 = cross0.magnitude();
        mag1 = cross1.magnitude();
        
       //^ for cross product, * for dot product
       //No collision if barycentric coords and their sums are not within [0,1] range.
        a = cross0 * (edge1 ^ (xi - tri.v0));
        a = a / (mag0*mag0);
        if ( (a<0) || (a>1) )
            { return false; } 

        b = cross1 * (edge0 ^ (xi - tri.v0));
        b = b / (mag1*mag1);
        if ( (b<0) || (b>1) )
            { return false; } 

        if ( (a+b < 0) || (a+b > 1) )
            { return false; } 

       return true;
    };

  //--------------------------------------
  Vector getTriNormal(Tri tri)
  {
      Vector e0, e1, n;
      e0 = tri.v1 - tri.v0;
      e1 = tri.v2 - tri.v0;
      n = e0 ^ e1;
      n.normalize();
      return n;
  }

  //--------------------------------------
  //Modify center-of-mass's velocity and angular velocity to reflect.
  //  Call after solveRB() detects a collision and sets 
  //angularVel_refl = angularVel + A*( inertia.inverse() * (ri cross np) )
  //Conservation of energy to find the constant A.
  void setNewVelocities(RigidBodyState &rbState, int collParticle, Tri tri)
  {
      Matrix I;
      Vector xcm, vcm, w, vcm_r, w_r, np, ri, q;
      double M, numer, denom, A, dampen;

      xcm = rbState->getCenterOfMassPos();
      vcm = rbState->getCenterOfMassVel();
      w = rbState->getAngularVel();
      I = rbState->getMomentOfInertia();
      M = rbState->getTotalMass();
      np = getTriNormal(tri);
      ri = rbState->getRelVertPos(collParticle);

      q = I.inverse() * (ri ^ np);
      numer = -1.0 * ( 2*vcm*np + q*I*w + w*I*q );
      denom = (1.0/M) + (q*I*q);
      A = numer/denom;  //denom shouldn't be 0 when using this conservation of energy (unlike with conservation of momentum).

// From HW1:  vr = stickiness*v_perp - restitution*np*(np*v);
//              where v_perp = v - np*(np*v);
// OR... vr = stickiness*(v-np*(np*v)) - restitution*np*(np*v)
//          = stickiness*v - stickiness*(np*(np*v)) - restitution*(np*(np*v))
//          = stickiness*v - (stickiness+restitution)*(np*(np*v))
// OR... vr = stickiness*v + (stickiness+restitution)*0.5*A*np  (similar to this project's setup)
//         where A = -2*(np*v)

      dampen = (restitution+stickiness)*0.5;
      vcm_r = stickiness*vcm + dampen*(A*np)/M;
      w_r = stickiness*w + dampen*A*q;

      //rbState->setCenterOfMassVel( vcm + (A*np)/M );
      //rbState->setAngularVel( w + A*q );
      rbState->setCenterOfMassVel(vcm_r);
      rbState->setAngularVel(w_r);
  }

  //--------------------------------------
  void solveRB(double dt, RigidBodyState &rbState, Mesh &mesh)
  {
      int numParticles, numTris, i, collParticle(-1), tr, hitCount(0);
      double mag, f0, f1, ff, dt_i(-1.0); 
      double t0, t1, eps(0.0001), max_dt_i(-1.0); 
      Vector xi, pi, xp, np, x_coll;
      Vector xcm, vcm, axis;
      Tri *tris, tri, hitTri;
      Matrix R, U;
      bool collide = true; 
      bool converged = false;

      numParticles = rbState->nb();
      numTris = mesh.getNumFaces(); 
      tris = mesh.getTris();

    //Move entire rigid body to positions at next dt.
      rbState->updateCenterOfMassPos(dt);
      rbState->updateRotationMat(dt);

      R = rbState->getRotationMat();
      axis = rbState->getAngularVel();
      mag = axis.magnitude();
      axis.normalize(); 

      xcm = rbState->getCenterOfMassPos();
      vcm = rbState->getCenterOfMassVel();

    //Step 1: Loop over particles and triangles, and detect collisions.
      for (i=0; i<numParticles; i++)
      {
        xi = rbState->getVertPos(i);  //New pos *after* the rigid body motion.
        pi = rbState->pos(i);
        for (tr=0; tr<numTris; tr++)
        {
            tri = tris[tr];
            xp = tri.v0;
            np = getTriNormal(tri);

            f0 = (xi - xp)*np;

          //Going backwards by the rotation change made in updateRotationMat().
            U = rotation(axis, mag*dt) * R;
            f1 = ((U*pi + xcm - vcm*dt) - xp) * np;

          //No collision if q0 == 0. 
            if ( f0 == 0.0 )
              { collide = false; }

          //If on plane at prev timestep (q1 == 0), then collision at the full time to go backwards, dt.
            if ( f1 == 0.0 )
              { dt_i = dt; } 

            if ( f0*f1 > 0.0 )
              { collide = false; }

      //----------
            if (collide)
            {
        //Find dt_i by rotating backwards until converging. Solving dt_i
        //  is not linear anymore because dt is in both q1 and U terms.
              t0 = 0.0;
              t1 = dt;
              dt_i = (t0+t1)/2.0;
              while (!converged)
              {
                U = rotation(axis, mag*dt_i) * R;
                ff = ( (U*pi + xcm - vcm*dt_i) - xp) * np;

                if (ff == 0.0)
                { 
                    converged = true; 
                    break;
                }
              //Adjust guessing to the upper half of the range. 
                else if (ff*f0 < 0.0)
                {
                   f1 = ff;
                   t1 = dt_i;  
                }
              //Adjust to the lower half. 
                else
                {
                   f0 = ff;
                   t0 = dt_i;
                }


                if ( abs( (t0-t1)/dt ) < eps)
                { 
                    converged = true; 
                    break;
                }

                dt_i = (t0+t1)/2.0;
              }  //end while(!converged)

          //printf("dt_i = %f\n", dt_i);
            } //end if(collide)

      //----------
        //Sanity check in case values had gotten too close to zero:
          if (dt*dt_i < 0.0)
              { collide = false; }
          if (dt_i > dt)
              { collide = false; }
          if ( ((dt-dt_i)/dt) == 0.0 )
              { collide = false; }

      //----------
        //If a dt_i was found, see if it's within the triangle.  Update first collision, if so.
          if (collide)
          {
              if (converged)
              {
                  x_coll = U*pi + xcm - vcm*dt_i;
                  if (detectCollWithTri(x_coll, tri))
                  {
                      if (dt_i > max_dt_i)
                      { 
                           max_dt_i = dt_i; 
                           hitTri = tri;        
                           collParticle = i;
                           hitCount++;
                      }
                  }
              }
          }

      //----------
          collide = true;  //Reset for next check.
          converged = false; 
        }  //end for tris

      }  //end for particles

      //----------
    //If there was a collision, reflect the rigid body.
      if (hitCount > 0)
      {
      //Step 2: Move RB state back to the collision moment.
          rbState->setCenterOfMassPos( xcm - vcm*dt_i );
          rbState->setRotationMat( rotation(axis, mag*dt_i)*R );

      //Step 3: Modify center-of-mass's vel and angular vel to reflect.
          setNewVelocities(rbState, collParticle, hitTri);

      //Step 4: Move RB state by dt_i, going forward in time after the reflection (negative mag).
          xcm = rbState->getCenterOfMassPos();
          vcm = rbState->getCenterOfMassVel();
          axis = rbState->getAngularVel();
          mag = axis.magnitude();
          axis.normalize();
          R = rbState->getRotationMat();

          rbState->setCenterOfMassPos( xcm + vcm*dt_i );
          rbState->setRotationMat( rotation(axis, -mag*dt_i)*R );

          rbState->set_ci( collParticle, Color(1.0,1.0,1.0,0.0) );    //Turn hit particle white.

      //Step 5: Repeat until no more collision, using dt_i as the timestep.
      //  Decrease dt_i until no more collisions could be detected.
          solveRB( dt_i-eps, rbState, mesh ); 

      }  //End if (hitCount>0)
  };

  //--------------------------------------
  private:
    double restitution;
    double stickiness;

};  //End class

}  //End namespace

#endif
