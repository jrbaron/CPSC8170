//-------------------------------------------------------
//
//  VelPartialSolver.h
//  A partial solver class for velocity.
//     S(v)(dt) {x,v} = {x, (v + f(x,v)/m *dt)}   //Update vel by a timestep of accel.
//  Also implements downward gravity.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

//class VelPartialSolver : public SolverThing
class VelPartialSolver
{
  public:
    VelPartialSolver() :
      gravMagnitude(1.0)
    { };

    ~VelPartialSolver() {};

  //--------------------------------------
    void setGravMagnitude(double g)
        { gravMagnitude = g; };

  //--------------------------------------
    void solveRB(double dt, RigidBodyState &rbState, Mesh &mesh)
    {
        Vector frc;
        Vector g(0, gravMagnitude*-9.8, 0);     
        double m;

        m = rbState->getTotalMass();
        frc = m*g;
        rbState->updateCenterOfMassVel(frc, dt);
    };

  //--------------------------------------
  private:
    double gravMagnitude;

};  //End class

}  //End namespace
