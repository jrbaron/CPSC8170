//-------------------------------------------------------
//
//  Mesh.h
//  A mesh class to parse an OBJ file and maintain its data.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>    //malloc(), calloc(), rand()
#include <fcntl.h>     //open(), read(), write(), close()
#include <string.h>    //For atoi() with fgets()

#ifdef __APPLE__
    #include <OpenGL/gl.h>     //For the GLfloat and GLuint types
#else
    #include <GL/gl.h>    
#endif

#include "Vector.h"


#ifndef __MESH_H__
#define __MESH_H__


struct Tri
{ pba::Vector v0, v1, v2; };

class Mesh
{
  public:
    Mesh(char *fname, int edges);
    ~Mesh();

    Tri* getTris();
    pba::Vector* getPbaVectors();
    GLfloat* getVertexArray();
    int getNumFaces();
    int getNumVertices();

  private:
    void parseObj();

    char *filename;
    int numEdges, numFaces, numVerts, numNormals, numTexCoords;
    GLfloat *vertices, *finalVertices, *finalNormals, *finalTexCoords;

};  //End class

#endif 
