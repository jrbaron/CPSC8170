//-------------------------------------------------------
//
//  LeapfrogSolver.h
//  A solver class implementing the Leapfrog approach. 
//    S(lf)(dt) = S(x)(dt/2) S(v)(dt/2) S(x)(dt/2) 
//
//    Composite solver of partial solvers on position, velocity, position.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class LeapfrogSolver : public SolverThing
{
  public:
    LeapfrogSolver() :
        SolverThing("Leapfrog"),
        posSolver(),
        velSolver()
        //posSolver( pba::createPosPartialSolver() ),
        //velSolver( pba::createVelPartialSolver() )
    { };

    ~LeapfrogSolver() 
    { };

  //--------------------------------------
  //Inherited method.
    void setGravMagnitude(double g)
        { velSolver.setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
    void setRestitution(double r)
        { posSolver.setRestitution(r); }

  //--------------------------------------
  //Inherited method.
    void setStickiness(double s)
        { posSolver.setStickiness(s); }

  //--------------------------------------
    void solve(double dt, RigidBodyState &rbState, Mesh &mesh)
    {
        posSolver.solveRB(dt/2, rbState, mesh);
        velSolver.solveRB(dt, rbState, mesh);
        posSolver.solveRB(dt/2, rbState, mesh);
    };

  //--------------------------------------
  private:
    PosPartialSolver posSolver; 
    VelPartialSolver velSolver; 

};  //End class


pba::Solver createLeapfrogSolver()
{
    return Solver( new pba::LeapfrogSolver() );
}

}  //End namespace
