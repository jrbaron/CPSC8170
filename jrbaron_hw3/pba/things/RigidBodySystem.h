//-------------------------------------------------------
//
//  RigidBodySystem.h
//  PbaThing for a system handling rigid bodies, implementing them 
//    as collections of particles with certain properties keeping
//    them together while rotating and translating the entire group. 
//
//  Jessica Baron 
//  CPSC 8170: Homework 3
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define PI 3.14159265  

#include "PbaThing.h"
#include "RigidBodyState.h"

#include "Solver.h"
#include "Mesh.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif

namespace pba
{


class RigidBodySystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    RigidBodySystem(const std::string nam="RigidBodySystem") :
        PbaThingyDingy (nam),
        solver( pba::createSixthOrderSolver() ),
        rbState( pba::createRigidBodyState(nam) ),

        //mesh( new Mesh("models/larger/Cube_large.obj", 3) ),    //3 for num of edges; using triangles.
        mesh( new Mesh("models/larger/Cube_medium.obj", 3) ),  

        rbMesh( new Mesh("models/smaller/smallsphere.obj", 3) ),  
       // rbMesh( new Mesh("models/smaller/littleCube.obj", 3) ),  
       // rbMesh( new Mesh("models/smaller/torus.obj", 3) ),  

        numParticles(-1),      //Should be set by the mesh.
        gravMag(0.3),    //Initially turn off so can observe Boids forces more.
        velMag(7.0),
        angVelMag(0.0),
        restitution(1.0), 
        stickiness(1.0)
    {
        std::cout << "Making a " << name << std::endl; 

        dt = 0.02;    //Start small.

        numParticles = rbMesh->getNumVertices();
        rbState->add(numParticles);	
        printf("num verts in rbMesh: %d\n", numParticles);

        setupState();
        solver->setGravMagnitude(gravMag);

        std::cout << name << " constructed\n";
    };
//--------------------------------------------------------
    ~RigidBodySystem()
    {
        delete mesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {

    }

    //--------------------------------------------------------
    void setupState()
    {
        int i;
        double per, random, yellow; 
        Vector *meshVerts;

        meshVerts = rbMesh->getPbaVectors();

        random = (rand()%50)/50.0;
	for (i=0; i<numParticles; i++)
	{
          per = (i*1.0)/numParticles;
          yellow = ((1-per)*0.5)+0.5;
          rbState->set_id(i, i);
          rbState->set_pos(i, meshVerts[i]);	
          rbState->set_mass(i, 1.0);
          rbState->set_ci(i, pba::Color(yellow*2, yellow*0.7, random*0.2, 0));
	}

      rbState->init(velMag, angVelMag);
    }
   
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Color meshColor(0.8, 0.3, 0.3, 0);
        Vector pos;

    //Draw particles as points.
        glPointSize(5.0);
        glBegin(GL_POINTS);
        for(int i=0; i<numParticles; i++)
        {
            pos = rbState->getVertPos(i);
          
            glColor3f( rbState->ci(i).red(), rbState->ci(i).green(), rbState->ci(i).blue() ); 
            glVertex3f( pos.X(), pos.Y(), pos.Z() );
        }
        glEnd();

    //Draw mesh to collide with.
        glColor3f( meshColor.red(), meshColor.green(), meshColor.blue() ); 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
        glEnableClientState(GL_VERTEX_ARRAY);
        glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );

    };

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve(), and this class already created a dt.
    void solve()
    {
        solver->solve(dt, rbState, *mesh);
    };

    //-------------------------------
    void Reset()
    {
     //Currently no way of cleaning up state and resetting num of particles.
     //Just move all particles back to origin with new starting positions and velocities.
        setupState();
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "===" << name << "===\n";
        std::cout << "p:     Print current parameter values.\n";
        std::cout << "v/V:   Decrease/increase the magnitude of the initial velocities.\n";
        std::cout << "g/G:     Reduce/increase magnitude of gravity.\n";
        std::cout << "c/C:     Reduce/increase coefficient of restitution in collisions.\n";
        std::cout << "s/S:     Reduce/increase coefficient of stickiness in collisions.\n";
        std::cout << "1/2:     Change solver to (1) Leapfrog or (2) Sixth Order using Leapfrog.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
        PbaThingyDingy::Keyboard(key, x, y);

        if (key == 'v')
        {
            velMag -= 0.01;
            std::cout << "Initial velocity magnitude: " << velMag << ".  Reset ('r') to see changes.\n";
        }
        else if (key == 'V')
        {
            velMag += 0.01;
            std::cout << "Initial velocity magnitude: " << velMag << ".  Reset ('r') to see changes.\n";
        }

        if (key == 'a')
        {
            angVelMag -= 0.01;
            std::cout << "Initial angular velocity magnitude: " << angVelMag << ".  Reset ('r') to see changes.\n";
        }
        else if (key == 'A')
        {
            angVelMag += 0.01;
            std::cout << "Initial angular velocity magnitude: " << angVelMag << ".  Reset ('r') to see changes.\n";
        }

      //Change which solver is being used.
        if (key == '1')
        { 
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        { 
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        {
            gravMag -= 0.01;
            if (gravMag <= 0.01) { gravMag = 0.0; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }
        else if (key == 'G')
        {
            gravMag += 0.01;

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }

   //Change stickiness but keep in range [0.0, 1.0].
        if (key == 's')
        {
            stickiness -= 0.01;
            if (stickiness < 0) { stickiness = 0.0; }    //Prevent from being negative.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }
        else if (key == 'S')
        {
            stickiness += 0.01;
            if (stickiness > 1) { stickiness = 1.0; }    //Prevent from exceeding 1.0.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }

     //Change restitution but keep in range [0.0, 1.0].
        if (key == 'c')
        {
            restitution -= 0.01;
            if (restitution < 0) { restitution = 0.0; }    //Prevent from being negative.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }
        else if (key == 'C')
        {
            restitution += 0.01;
            if (restitution > 1) { restitution = 1.0; }    //Prevent from exceeding 1.0.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }

     //Print current user parameters.
        if (key == 'p')
        {
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
            std::cout << "Initial velocities magnitude: " << velMag << "\n";
            std::cout << "Gravity magnitude:\t" << gravMag << "\n";
            std::cout << "Coeff. of restitution:\t" << restitution << "\n";
            std::cout << "Coeff. of stickiness:\t" << stickiness << "\n\n";
            rbState->print();
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
        }
    };

  //--------------------------------------------------------
  private:
    Solver solver;
    RigidBodyState rbState;
    Mesh *mesh, *rbMesh;
    int numParticles;
    double gravMag, velMag, angVelMag; 
    double restitution, stickiness;

}; 	//End class declaration



//--------------------------------------------------------
pba::PbaThing makeRigidBodySystem()
{
    return PbaThing( new pba::RigidBodySystem() );
}


}  //End namespace pba



#endif
