//-------------------------------------------------------
//
//  Mesh.h
//  A mesh class to parse an OBJ file and maintain its data.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>    //malloc(), calloc(), rand()
#include <fcntl.h>     //open(), read(), write(), close()
#include <string.h>    //For atoi() with fgets()

#ifdef __APPLE__
    #include <OpenGL/gl.h>     //For the GLfloat and GLuint types
#else
    #include <GL/gl.h>    
#endif

#include "Vector.h"


#ifndef __MESH_H__
#define __MESH_H__


struct Tri
{ pba::Vector v0, v1, v2; };

class Mesh
{
  public:
    Mesh(char *fname, int edges) : 
        filename(fname), 
        numEdges(edges),
        numFaces(0), 
        numVerts(0), 
        numNormals(0), 
        numTexCoords(0),
        finalVertices(NULL), 
        finalNormals(NULL), 
        finalTexCoords(NULL)
    {
        parseObj();
    };

  //-------------------------------------------
    ~Mesh() 
    {
        if (finalVertices != NULL)
            { free(finalVertices); }

        if (finalNormals != NULL)
            { free(finalNormals); }

        if (finalTexCoords != NULL)
            { free(finalTexCoords); }

    };

  //-------------------------------------------
  //Returns array of Tri structs for the PBA classes to use.
    Tri* getTris()
    { 
        if (numEdges != 3)
            { printf("Mesh::getTris(): ERROR: numEdges not set to 3 for Mesh instance.\n"); }

        Tri *triangles;
        int i, v;
        triangles = (struct Tri*) malloc(numFaces * sizeof(struct Tri));
        for (i=0; i<numFaces; i++)
        {
            v = i*numEdges*3;   
            triangles[i].v0.set(finalVertices[v], finalVertices[v+1], finalVertices[v+2] );
            triangles[i].v1.set(finalVertices[v+3], finalVertices[v+4], finalVertices[v+5] );  
            triangles[i].v2.set(finalVertices[v+6], finalVertices[v+7], finalVertices[v+8] );  
        }
        
        return triangles; 
    };

  //-------------------------------------------
  //Returns array of Tri structs for the PBA classes to use.
    GLfloat* getVertexArray()
    { 
        return finalVertices; 
    };

  //-------------------------------------------
    int getNumFaces()
    {
        return numFaces;
    };

  //-------------------------------------------
  private:
  //Parses an OBJ file and updates global geometry arrays (vertices, normals, etc.).
    void parseObj()
    {
        FILE *fptr;
        char *parse;
        int lineLength = 1024, i;
        int vertInd = 0, texCoordInd = 0, normalInd = 0, faceInd = 0;
        char buf[lineLength];
        GLfloat *vertices, *normals, *texCoords;
        GLuint *faceVerts, *faceNormals, *faceTexCoords;

        fptr = fopen(filename, "r");    if (!fptr) printf("Could not load file %s as OBJ.\n", filename);
        while ( fgets(buf, lineLength, fptr) )          //First get all counts.
        {
                if (buf[0] == 'v')
                {
                        if (buf[1] == ' ')      numVerts++;
                        else if (buf[1] == 't') numTexCoords++;
                        else if (buf[1] == 'n') numNormals++;
                }
                else if (buf[0] == 'f') numFaces++;
        }
        rewind(fptr);                   //Return pointer to beginning of file.

        vertices = (GLfloat *) malloc(numVerts * 3 * sizeof(GLfloat));
        texCoords = (GLfloat *) malloc(numTexCoords * 2 * sizeof(GLfloat));
        normals = (GLfloat *) malloc(numNormals * 3 * sizeof(GLfloat));

        faceVerts = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));            //This saves just vertex indices. 
        faceTexCoords = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));        //Faces also have vt and vn info.       
        faceNormals = (GLuint*) malloc(numFaces * numEdges * sizeof(GLuint));

        //printf("Num verts: %d \nNum edges per face: %d \nNum faces: %d \nNum texcoords %d \nNum normals %d \n",
        //        numVerts, numEdges, numFaces, numTexCoords, numNormals);

        while ( fgets(buf, lineLength, fptr) )
        {
                if (buf[0] == 'v')
                {
                        if (buf[1] == ' ')
                        {
                                parse = strtok(buf, " \t");     //Strip 'v'.
                                parse = strtok(NULL, " \t");    //x-coord
                                vertices[vertInd] = atof(parse);
                                parse = strtok(NULL, " \t");    //y-coord
                                vertices[vertInd+1] = atof(parse);
                                parse = strtok(NULL, " \t\n");  //z-coord
                                vertices[vertInd+2] = atof(parse);

                                vertInd += 3;                   //3 coords per vertex
                        }
                        else if (buf[1] == 't')
                        {
                                parse = strtok(buf, " \t");     //Strip 'vt'.
                                parse = strtok(NULL, " \t");    //s-coord
                                texCoords[texCoordInd] = atof(parse);
                                parse = strtok(NULL, " \t\n");  //t-coord
                                texCoords[texCoordInd+1] = atof(parse);
                                texCoordInd += 2;               //2 coords per tex coord        
                        }
                        else if (buf[1] == 'n')
                        {
                                parse = strtok(buf, " \t");     //Strip 'vn'.
                                parse = strtok(NULL, " \t");    //x-coord
                                normals[normalInd] = atof(parse);
                                parse = strtok(NULL, " \t");    //y-coord
                                normals[normalInd+1] = atof(parse);
                                parse = strtok(NULL, " \t\n");  //z-coord
                                normals[normalInd+2] = atof(parse);
                                normalInd += 3;                 //3 coords per normal 
                        }
                }

                else if (buf[0] == 'f')
                {
                    parse = strtok(buf, " \t");             //Strip 'f'.
                    for (i=0; i<numEdges; i++)
                    {
                        if ( (numTexCoords != 0) && (numNormals != 0) )
                        {
                            parse = strtok(NULL, "/");                      //Vertex index of i-th face vertex
                            faceVerts[faceInd+i] = atoi(parse)-1;           //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
                            parse = strtok(NULL, "/");                      //Tex coords index of i-th face vertex
                               
                            faceTexCoords[faceInd+i] = atoi(parse)-1;        //-1!
                            if (i==numEdges)
                                { parse = strtok(NULL, " \t\n"); }              //Normal index of LAST face vertex (at end of line)
                            else
                                { parse = strtok(NULL, " \t"); }                //Normal index of i-th face vertex

                            faceNormals[faceInd+i] = atoi(parse) -1; 
                        }
           
                        else
                        {
                            if (i==numEdges)
                                { parse = strtok(NULL, " \t\n"); }              //Vertex index of LAST face vertex (at end of line)
                            else
                                { parse = strtok(NULL, " \t"); }                //Vertex index of i-th face vertex
                            //printf("\tparse: %s\n", parse);
                            faceVerts[faceInd+i] = atoi(parse)-1;               //-1 because of OBJ indexing! OpenGL will use buffers with indexing starting at 0.
 
                        }
                    }
                    faceInd += numEdges;

                }
        }

        fclose(fptr);


   //Approach without GL element arrays. (DrawElements does not allow different indices for vertices and normals.)
        finalVertices = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat)); //4 verts per face * components (3 or 2) per item
        finalNormals = (GLfloat *) malloc(numEdges * numFaces * 3 * sizeof(GLfloat));
        finalTexCoords = (GLfloat *) malloc(numEdges * numFaces * 2 * sizeof(GLfloat));

        int fVert, indV, indN, indTex;
        for(fVert=0; fVert<numFaces*numEdges; fVert++)
        {
            indV = faceVerts[fVert];
            for (i=0; i<3; i++)
            {
                finalVertices[fVert*3+i] = vertices[indV*3+i];
            }

           if ( (numNormals != 0) && (numTexCoords != 0) )
           {
                indN = faceNormals[fVert];
                indTex = faceTexCoords[fVert];
                for (i=0; i<3; i++)
                {
                    finalNormals[fVert*3+i] = normals[indN*3+i];
                }
                for (i=0; i<2; i++)
                {
                    finalTexCoords[fVert*2+i] = texCoords[indTex*2+i];
                }
            }
           
        }

        free(vertices);
        free(normals);
        free(texCoords);
        free(faceVerts);
        free(faceNormals);
        free(faceTexCoords);
    }

  //-------------------------------------------
  // Private data
    char *filename;
    int numEdges, numFaces, numVerts, numNormals, numTexCoords;
    GLfloat *finalVertices, *finalNormals, *finalTexCoords;

};  //End class

#endif 
