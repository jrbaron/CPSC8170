//-------------------------------------------------------
//
//  GravityCollisionSystem.h
//  PbaThing for a system handling gravitational and collision forces. 
//
//  Jessica Baron 
//  CPSC 8170: Homework 1
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__


#include "PbaThing.h"
#include "DynamicalState.h"

#include "Solver.h"
#include "Force.h"     //Currently not used but base class created for future expansion.
                       //Downward gravity is implemented in the VelPartialSolver class.
#include "Mesh.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "ForwardEulerSolver.h"	//For testing.
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif


namespace pba
{


class GravityCollisionSystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    GravityCollisionSystem(const std::string nam="GravityCollisionSystem") :
        PbaThingyDingy (nam),
        solver( pba::createLeapfrogSolver() ),
        state( pba::CreateDynamicalState(nam) ),
        mesh( new Mesh("bigsphere.obj", 3) ),    //3 for num of edges; using triangles.
        numParticles(1),
        gravMagnitude(0.5),
        restitution(0.9),
        stickiness(0.3)
    {
        std::cout << "Making a " << name << std::endl; 

        state->add(numParticles);	
        Reset();

        std::cout << name << " constructed\n";
    };

    //--------------------------------------------------------
    ~GravityCollisionSystem()
    {
        delete mesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {
        setupState();
        //printTris( mesh->getTris() );
    }

    //--------------------------------------------------------
    void setParticleProps(int id, double percent = (rand()%50)/50.0)
    {
            double random = (rand()%50)/50.0;
            state->set_id(id, id);
            state->set_pos(id, pba::Vector(rand()%4-2, rand()%4-2, rand()%4-2));	
            state->set_vel(id, pba::Vector(rand()%4-2, rand()%4-2, rand()%4-2));
            state->set_accel(id, pba::Vector(0,0,0));
            state->set_mass(id, 1.0);
            state->set_ci(id, pba::Color(percent, random*0.4, 1-percent, 0));
    }

    //--------------------------------------------------------
    void setupState()
    {
        int i;
        double per; 
	for (i=0; i<numParticles; i++)
	{
            per = (i*1.0)/numParticles;
            setParticleProps(i, per);
	}
    }	//No semicolon? Alread declared in PbaThingyDingy?
   
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Color meshColor(0.7, 0.7, 1.0, 0);

    //Draw particles as points.
        glPointSize(5.0);
        glBegin(GL_POINTS);
        for(int i=0; i<numParticles; i++)
        {
            glColor3f( state->ci(i).red(), state->ci(i).green(), state->ci(i).blue() ); 
            glVertex3f( state->pos(i).X(), state->pos(i).Y(), state->pos(i).Z() );

            //std::cout << "Particle pos at i=" << i << ": " << state->pos(i).X() << state->pos(i).Y() << state->pos(i).Z() << std::endl;
        }
        glEnd();

    //Draw mesh to collide with.
        glColor3f( meshColor.red(), meshColor.green(), meshColor.blue() ); 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
        glEnableClientState(GL_VERTEX_ARRAY);
        glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );

    };

    //-------------------------------
    //Function for testing to make sure Mesh data can be accessed here.
    void printTris(Tri* tris)
    {
        for (int i=0; i<mesh->getNumFaces(); i++)
        {
            std::cout << "i = " << i << std::endl;
            std::cout << "v0: " << tris[i].v0.X() << ", " << 
                tris[i].v0.Y() << ", " << tris[i].v0.Z() << std::endl;
            std::cout << "v1: " << tris[i].v1.X() << ", " << 
                tris[i].v1.Y() << ", " << tris[i].v1.Z() << std::endl;
            std::cout << "v2: " << tris[i].v2.X() << ", " << 
                tris[i].v2.Y() << ", " << tris[i].v2.Z() << std::endl;
            std::cout << "---\n";
        }

        printf("\n\n");
    }

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve().
    void solve()
    {
        solver->solve(dt, state, *mesh);		//PbaThingyDingy already created a dt.
       // printf("New vel: %f, %f, %f\n", state->vel(0).X(), state->vel(0).Y(), state->vel(0).Z());
    };

    //-------------------------------
    void Reset()
    {
        //state = pba::CreateDynamicalState(name);    //Make a new one in case numParticles had changed?

        dt = (1.0)/(24.0);
        numParticles = 1;
        gravMagnitude = 0.5;
        restitution = 0.9;
        stickiness = 0.3;
        solver->setGravMagnitude(gravMagnitude);
        solver->setRestitution(restitution);
        solver->setStickiness(stickiness);
        setupState();
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "===" << name << "===\n";
        std::cout << "e:     Start/stop emitting particles.\n";
        std::cout << "g/G:     Reduce/increase magnitude of gravity.\n";
        std::cout << "c/C:     Reduce/increase coefficient of restitution in collisions.\n";
        std::cout << "s/S:     Reduce/increase coefficient of stickiness in collisions.\n";
        std::cout << "1/2/3:     Change solver to (1) Leapfrog, (2) Sixth Order using Leapfrog, or (3) Forward Euler.\n";
        std::cout << "4/5/6:     Change mesh to (3) Triangulated Cube, (4) Big Sphere, or (5) Plane.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
      //Emit another particle.
        if (key == 'e')
        { 
            int id = numParticles;
            numParticles++; 
            state->add(1);
            setParticleProps(id);
            std::cout << "Emitting a particle. Count = " << numParticles << ".\n";
        }

      //Change which solver is being used.
        if (key == '1')
        { 
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        { 
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }
        else if (key == '3')
        { 
            solver = pba::createForwardEulerSolver();
            Reset();
            std::cout << "Solver changed to Forward Euler.\n";
        }

      //Change which mesh is loaded.
        if (key == '4')
        { 
            delete mesh;
            mesh = new Mesh("Cube_Tris.obj", 3),
            Reset();
            std::cout << "Mesh changed to Triangulated Cube.\n";
        }
        else if (key == '5')
        { 
            delete mesh;
            mesh = new Mesh("bigsphere.obj", 3),
            Reset();
            std::cout << "Mesh changed to Big Sphere.\n";
        }
        else if (key == '6')
        { 
            delete mesh;
            mesh = new Mesh("plane.obj", 3),
            Reset();
            std::cout << "Mesh changed to Plane.\n";
        }

     //Change timestep but keep positive.
        if (key == 't')
        { 
            dt -= 0.001;
            if (dt < 0.0001) { dt = 0.0001; }         //Prevent from being negative.
            std::cout << "Timestep: " << dt << "\n";
        }
        else if (key == 'T')
        { 
            dt += 0.001;
            std::cout << "Timestep: " << dt << "\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        { 
            gravMagnitude -= 0.01;
            if (gravMagnitude < 0.0001) { gravMagnitude = 0.0001; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMagnitude);
            std::cout << "Gravity magnitude: " << gravMagnitude << "\n";
        }
        else if (key == 'G')
        { 
            gravMagnitude += 0.01;

            solver->setGravMagnitude(gravMagnitude);
            std::cout << "Gravity magnitude: " << gravMagnitude << "\n";
        }

     //Change stickiness but keep in range [0.0, 1.0].
        if (key == 's')
        {
            stickiness -= 0.01;
            if (stickiness < 0) { stickiness = 0.0; }    //Prevent from being negative.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }
        else if (key == 'S')
        {
            stickiness += 0.01;
            if (stickiness > 1) { stickiness = 1.0; }    //Prevent from exceeding 1.0.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }

     //Change restitution but keep in range [0.0, 1.0].
        if (key == 'c')
        {
            restitution -= 0.01;
            if (restitution < 0) { restitution = 0.0; }    //Prevent from being negative.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }
        else if (key == 'C')
        {
            restitution += 0.01;
            if (restitution > 1) { restitution = 1.0; }    //Prevent from exceeding 1.0.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }

    };

  //--------------------------------------------------------
  private:
    Solver solver;
    DynamicalState state;
    Mesh *mesh;
    int numParticles;
    double gravMagnitude, restitution, stickiness;


}; 	//End class declaration



//--------------------------------------------------------
//Shared pointer to a GravityCollisionSystem object so it's viewed as a PbaThing.
//This is used in order to avoid Python's garbage collection cleaning up data when it's still needed.
//It's is called in Python to create a GravityCollisionSystem.
//It's created here and not in the header file in order to actually instantiate an object after the class definition.

//Note that this is a METHOD and should have a different name from the CLASS defined here.
pba::PbaThing makeGravityCollisionSystem()
{
    return PbaThing( new pba::GravityCollisionSystem() );
}


}	//End namespace pba



#endif
