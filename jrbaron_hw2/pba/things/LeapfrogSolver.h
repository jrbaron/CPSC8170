//-------------------------------------------------------
//
//  LeapfrogSolver.h
//  A solver class implementing the Leapfrog approach. 
//    S(lf)(dt) = S(x)(dt/2) S(v)(dt/2) S(x)(dt/2) 
//
//    Composite solver of partial solvers on position, velocity, position.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class LeapfrogSolver : public SolverThing
{
  public:
    LeapfrogSolver() :
        SolverThing("Leapfrog"),
        posSolver( pba::createPosPartialSolver() ),
        velSolver( pba::createVelPartialSolver() )
    {

    };

    ~LeapfrogSolver() {};

  //--------------------------------------
  //Inherited method.
    void setGravMagnitude(double g)
        { velSolver->setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
    void setRestitution(double r)
        { posSolver->setRestitution(r); }

  //--------------------------------------
  //Inherited method.
    void setStickiness(double s)
        { posSolver->setStickiness(s); }

  //--------------------------------------
    void solve(double dt, DynamicalState &state, Mesh &mesh, BoidsForces &boidsForces)
    {
        posSolver->solve(dt/2, state, mesh, boidsForces);
        velSolver->solve(dt, state, mesh, boidsForces);
        posSolver->solve(dt/2, state, mesh, boidsForces);
    };

  //--------------------------------------
  private:
    Solver posSolver; 
    Solver velSolver; 

};  //End class


pba::Solver createLeapfrogSolver()
{
    return Solver( new pba::LeapfrogSolver() );
}

}  //End namespace
