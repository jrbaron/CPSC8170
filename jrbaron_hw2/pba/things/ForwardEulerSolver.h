//-------------------------------------------------------
//
//  ForwardEulerSolver.h
//  A solver class implementing the Leapfrog approach. 
//    S(fe) = S(v)(dt) S(x)(dt)) 
//
//    Composite solver of partial solvers on velocity and position.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

// Gravity: F{x,v} = g*m, 	g = (0, -9.8, 0)
//	or 1 unit in space is 10 meters.

namespace pba
{

class ForwardEulerSolver : public SolverThing
{
  public:
    ForwardEulerSolver() :
        SolverThing("ForwardEuler"),
        posSolver( pba::createPosPartialSolver() ),
        velSolver( pba::createVelPartialSolver() )
    {

    };

    ~ForwardEulerSolver() {};

  //--------------------------------------
  //Inherited method.
    void setGravMagnitude(double g)
        { velSolver->setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
    void setRestitution(double r)
        { posSolver->setRestitution(r); }

  //--------------------------------------
  //Inherited method.
    void setStickiness(double s)
        { posSolver->setStickiness(s); }

  //--------------------------------------
    void solve(double dt, DynamicalState &state, Mesh &mesh, BoidsForces &boidsForces)
    {
        posSolver->solve(dt, state, mesh, boidsForces);
        velSolver->solve(dt, state, mesh, boidsForces);
    };

  //--------------------------------------
  private:
    Solver posSolver; 
    Solver velSolver; 

};  //End class


pba::Solver createForwardEulerSolver()
{
    return Solver( new pba::ForwardEulerSolver() );
}

}  //End namespace
