//-------------------------------------------------------
//
//  BoidFlockingSystem.h
//  PbaThing for a system handling Boids, implementing  
//    collision avoidance, velocity matching, and centering.
//
//  Jessica Baron 
//  CPSC 8170: Homework 2
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define PI 3.14159265    //Used for setting FoV in RADIANS.

#include "PbaThing.h"
#include "DynamicalState.h"

#include "Solver.h"
#include "Mesh.h"
#include "BoidsForces.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "ForwardEulerSolver.h"	//For testing.
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif


namespace pba
{


class BoidFlockingSystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    BoidFlockingSystem(const std::string nam="BoidsSystem") :
        PbaThingyDingy (nam),
        solver( pba::createLeapfrogSolver() ),
        state( pba::CreateDynamicalState(nam) ),
        mesh( new Mesh("models/bigsphere.obj", 3) ),    //3 for num of edges; using triangles.
        boidsForces(),
        numParticles(150),
        gravMagnitude(0.0),    //Initially turn off so can observe Boids forces more.
        avoidance(0.3),
        velocityMatching(2.0),
        centering(3.0),
        maxAccel(30.0),
        range(5.0),            //Distance between boids to be within where they're affected by one another.
        rangeRamp(0.2),
        fieldOfView(PI*0.5),
        fovRamp(0.2)
    {
        std::cout << "Making a " << name << std::endl; 

        solver->setGravMagnitude(gravMagnitude);
        setBoidsForcesParams();
        state->add(numParticles);	
        setupState();

        std::cout << name << " constructed\n";
    };

    //--------------------------------------------------------
    ~BoidFlockingSystem()
    {
        delete mesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {
        //printTris( mesh->getTris() );
    }

    //--------------------------------------------------------
    void setBoidsForcesParams()
    {
        boidsForces.setAvoidance(avoidance);
        boidsForces.setVelocityMatching(velocityMatching);
        boidsForces.setCentering(centering);
        boidsForces.setMaxAccel(maxAccel);
        boidsForces.setRange(range);
        boidsForces.setRangeRamp(rangeRamp);
        boidsForces.setFieldOfView(fieldOfView);
        boidsForces.setFovRamp(fovRamp);
    }

    //--------------------------------------------------------
    void setParticleProps(int id, double percent = (rand()%50)/50.0)
    {
        double random, notZeroY;
        random = (rand()%50)/50.0;

      //Making sure some val is not zero, using ternary operator. Assure no (0,0,0) 
      //  vector for velocity, as it creates NaNs during normalize() (in BoidsForces::calcVisibilityWeight()).
        notZeroY = rand()%4-2;
        notZeroY = (notZeroY==0) ? (notZeroY+0.5) : (notZeroY);  

        state->set_id(id, id);
        state->set_pos(id, pba::Vector(rand()%4-2, rand()%4-2, rand()%4-2));	
        state->set_vel(id, pba::Vector( rand()%4-2, notZeroY, rand()%4-2) ); 
        state->set_accel(id, pba::Vector(0,0,0));
        state->set_mass(id, 1.0);
        state->set_ci(id, pba::Color(random*0.4, percent, 1-percent, 0));
    }

    //--------------------------------------------------------
    void setupState()
    {
        int i;
        double per; 
	for (i=0; i<numParticles; i++)
	{
            per = (i*1.0)/numParticles;
            setParticleProps(i, per);
	}
    }
   
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Color meshColor(0.7, 0.7, 1.0, 0);

    //Draw particles as points.
        glPointSize(5.0);
        glBegin(GL_POINTS);
        for(int i=0; i<numParticles; i++)
        {
            glColor3f( state->ci(i).red(), state->ci(i).green(), state->ci(i).blue() ); 
            glVertex3f( state->pos(i).X(), state->pos(i).Y(), state->pos(i).Z() );
        }
        glEnd();

    //Draw mesh to collide with.
        glColor3f( meshColor.red(), meshColor.green(), meshColor.blue() ); 
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
        glEnableClientState(GL_VERTEX_ARRAY);
        glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );

    };

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve(), and this class already created a dt.
    void solve()
    {
        solver->solve(dt, state, *mesh, boidsForces);
    };

    //-------------------------------
    void Reset()
    {
        gravMagnitude = 0.0;
        solver->setGravMagnitude(0.0);
     //Currently no way of cleaning up state and resetting num of particles.
     //Just move all particles back to origin with new starting positions and velocities.
        setupState();
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "===" << name << "===\n";
        std::cout << "e:     Start/stop emitting particles.\n";
        std::cout << "p:     Print current parameter values.\n";
        std::cout << "a/A:     Reduce/increase collision avoidance strength of Boids.\n";
        std::cout << "v/V:     Reduce/increase velocity matching strength of Boids.\n";
        std::cout << "c/C:     Reduce/increase centering strength of Boids.\n";
        std::cout << "m/M:     Reduce/increase the maximum acceleration threshold.\n";
        std::cout << "d/D:     Reduce/increase the distance range to trigger Boids' effects on one another.\n";
        std::cout << "y/Y:     Reduce/increase a ramp to the boids' range.\n"; 
        std::cout << "u/U:     Reduce/increase a ramp to the boids' field-of-view.\n"; 
        std::cout << "q/Q:     Reduce/increase the boids' field-of-view.\n"; 
        std::cout << "1/2/3:     Change solver to (1) Leapfrog, (2) Sixth Order using Leapfrog, or (3) Forward Euler.\n";
        std::cout << "4/5/6:     Change mesh to (3) Triangulated Cube, (4) Big Sphere, or (5) Plane.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
      //Emit another particle.
        if (key == 'e')
        { 
            int id, i;
            id = state->add(10);  //Returns the index to the last particle.
            for (i=0; i<10; i++)
                { setParticleProps(i + numParticles); }
            numParticles = state->nb(); 
            std::cout << "Emitting a particle. Count = " << numParticles << ".\n";
        }

      //Change which solver is being used.
        if (key == '1')
        { 
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        { 
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }
        else if (key == '3')
        { 
            solver = pba::createForwardEulerSolver();
            Reset();
            std::cout << "Solver changed to Forward Euler.\n";
        }

      //Change which mesh is loaded.
        if (key == '4')
        { 
            delete mesh;
            mesh = new Mesh("models/Cube_Tris.obj", 3),
            Reset();
            std::cout << "Mesh changed to Triangulated Cube.\n";
        }
        else if (key == '5')
        { 
            delete mesh;
            mesh = new Mesh("models/bigsphere.obj", 3),
            Reset();
            std::cout << "Mesh changed to Big Sphere.\n";
        }
        else if (key == '6')
        { 
            delete mesh;
            mesh = new Mesh("models/plane.obj", 3),
            Reset();
            std::cout << "Mesh changed to Plane.\n";
        }

     //Change timestep but keep positive and nonzero.
        if (key == 't')
        { 
            dt -= 0.001;
            if (dt <= 0.0001) { dt = 0.0001; }         //Prevent from being negative.
            std::cout << "Timestep: " << dt << "\n";
        }
        else if (key == 'T')
        { 
            dt += 0.001;
            std::cout << "Timestep: " << dt << "\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        {
            gravMagnitude -= 0.01;
            if (gravMagnitude <= 0.01) { gravMagnitude = 0.0; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMagnitude);
            std::cout << "Gravity magnitude: " << gravMagnitude << "\n";
        }
        else if (key == 'G')
        {
            gravMagnitude += 0.01;

            solver->setGravMagnitude(gravMagnitude);
            std::cout << "Gravity magnitude: " << gravMagnitude << "\n";
        }

     //Change collision avoidance strength but keep positive.
        if (key == 'a')
        { 
            avoidance -= 0.01;
            if (avoidance <= 0.01) { avoidance = 0.0; }        

            boidsForces.setAvoidance(avoidance);
            std::cout << "Avoidance Strength: " << avoidance << "\n";
        }
        else if (key == 'A')
        { 
            avoidance += 0.01;

            boidsForces.setAvoidance(avoidance);
            std::cout << "Avoidance Strength: " << avoidance << "\n";
        }

     //Change velocity matching strength but keep positive.
        if (key == 'v')
        {
            velocityMatching -= 0.01;
            if (velocityMatching <= 0.01) { velocityMatching = 0.0; }        

            boidsForces.setVelocityMatching(velocityMatching);
            std::cout << "Velocity Matching Strength: " << velocityMatching << "\n";
        }
        else if (key == 'V')
        {
            velocityMatching += 0.01;

            boidsForces.setVelocityMatching(velocityMatching);
            std::cout << "Velocity Matching Strength: " << velocityMatching << "\n";
        }

     //Change centering strength but keep positive.
        if (key == 'c')
        {
            centering -= 0.01;
            if (centering <= 0.01) { centering = 0.0; }        

            boidsForces.setCentering(centering);
            std::cout << "Centering Strength: " << centering << "\n";
        }
        else if (key == 'C')
        {
            centering += 0.01;

            boidsForces.setCentering(centering);
            std::cout << "Centering Strength: " << centering << "\n";
        }

     //Change max acceleration threshold but keep positive (it's a magnitude).
        if (key == 'm')
        {
            maxAccel -= 0.5;
            if (maxAccel <= 0.0) { maxAccel = 0.0; }        

            boidsForces.setMaxAccel(maxAccel);
            std::cout << "Max Acceleration Magnitude: " << maxAccel << "\n";
        }
        else if (key == 'M')
        {
            maxAccel += 0.5;

            boidsForces.setMaxAccel(maxAccel);
            std::cout << "Max Acceleration Magnitude: " << maxAccel << "\n";
        }

     //Change boids' max range but keep non-negative.
        if (key == 'd')
        {
            range -= 0.1;
            if (range <= 0.1) { range = 0.0; }        

            boidsForces.setRange(range);
            std::cout << "Boids' Range: " << range << "\n";

        }
        else if (key == 'D')
        {
            range += 0.1;

            boidsForces.setRange(range);
            std::cout << "Boids' Range: " << range << "\n";
        }

     //Change boids' range ramp but keep positive.
        if (key == 'y')
        {
            rangeRamp -= 0.01;
            if (rangeRamp <= 0.01) { rangeRamp = 0.01; }        

            boidsForces.setRangeRamp(rangeRamp);
            std::cout << "Boids' Range Ramp/Modifier: " << rangeRamp << "\n";
        }
        else if (key == 'Y')
        {
            rangeRamp += 0.01;

            boidsForces.setRangeRamp(rangeRamp);
            std::cout << "Boids' Range Ramp/Modifier: " << rangeRamp << "\n";
        }

     //Change boids' field-of-view ramp but keep positive.
        if (key == 'u')
        {
            fovRamp -= 0.01;
            if (fovRamp <= 0.01) { fovRamp = 0.01; }        

            boidsForces.setFovRamp(fovRamp);
            std::cout << "Boids' Field-of-View Ramp/Modifier: " << fovRamp << "\n";
        }
        else if (key == 'U')
        {
            fovRamp += 0.01;

            boidsForces.setFovRamp(fovRamp);
            std::cout << "Boids' Field-of-View Ramp/Modifier: " << fovRamp << "\n";
        }

     //Change field-of-view but keep in (0 rad, 2PI rad]. (cmath.cos() uses radians.)
        if (key == 'q')
        {
            double pi16 = PI/16;
            fieldOfView -= pi16;    //Small fraction of pi.
            if (fieldOfView <= pi16) { fieldOfView = pi16; }        

            boidsForces.setFieldOfView(fieldOfView);
            std::cout << "Boids' Field-of-View: " << fieldOfView << "\n";
        }
        else if (key == 'Q')
        {
            fieldOfView += PI/16;
            if (fieldOfView >= 2*PI) { fieldOfView = 2*PI; }    //Cap at the full circle.

            boidsForces.setFieldOfView(fieldOfView);
            std::cout << "Boids' Field-of-View: " << fieldOfView << "\n";
        }

     //Print current user parameters.
        if (key == 'p')
        {
            std::cout << "Avoidance Strength: \t\t" << avoidance << "\n";
            std::cout << "Velocity Matching Strength: \t" << velocityMatching << "\n";
            std::cout << "Centering Strength: \t\t" << centering << "\n";
            std::cout << "Max Acceleration Magnitude: \t" << maxAccel << "\n";
            std::cout << "Boids' Range: \t\t\t" << range << "\n";
            std::cout << "Boids' Field-of-View (rads): \t" << fieldOfView << "\n";
            std::cout << "Boids' Range Ramp/Modifier (%): " << rangeRamp << "\n";
            std::cout << "Boids' FoV Ramp/Modifier (%): " << fovRamp << "\n";
        }
    };

  //--------------------------------------------------------
  private:
    Solver solver;
    DynamicalState state;
    Mesh *mesh;
    BoidsForces boidsForces;
    int numParticles;
    double gravMagnitude;
    double avoidance, velocityMatching;
    double centering, maxAccel;
    double range, rangeRamp;
    double fieldOfView, fovRamp;

}; 	//End class declaration



//--------------------------------------------------------
pba::PbaThing makeBoidFlockingSystem()
{
    return PbaThing( new pba::BoidFlockingSystem() );
}


}	//End namespace pba



#endif
