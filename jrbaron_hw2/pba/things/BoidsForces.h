//-------------------------------------------------------
//
//  BoidsForces.h
//  A class that handles accumulating forces and accelerations of boids objects. 
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __BOIDSFORCES_H__
#define __BOIDSFORCES_H__

#include <iostream>
#include <memory>    //Sometimes needed for Makefile to see the shared_ptr as a type?
#include "DynamicalState.h"

namespace pba
{

class BoidsForces 
{
  public:
    BoidsForces() {};
    //Do not initialize the class variables here since
    // they should be changed only by the BoidFlockingSystem.
    //It's deceptive if they have some hard-coded values in this file.
 
  //--------------------------
    ~BoidsForces() {};

  //--------------------------
    void setAvoidance(double a)
        { avoidance = a; }

  //--------------------------
    void setVelocityMatching(double vm)
        { velocityMatching = vm; }

  //--------------------------
    void setCentering(double c)
        { centering = c; }

  //--------------------------
    void setMaxAccel(double a)
        { maxAccel = a; }

  //--------------------------
    void setRange(double r)
        { range = r; }

  //--------------------------
    void setRangeRamp(double r)
        { rangeRamp = r; }

  //--------------------------
    void setFieldOfView(double fov)
        { fieldOfView = fov; }

  //--------------------------
    void setFovRamp(double ramp)
        { fovRamp = ramp; }

  //--------------------------
    pba::Vector calcTotalAccel(pba::DynamicalState &state, int boidID)
    {
      //Vectors initialized as (0,0,0).
        pba::Vector d, totalAccel, avoidAccel, velAccel, centerAccel;
        int count, other;
        double rangeWeight, visibility, residual;     //Used in keeping total accel under max accel.

        count = state->nb();
        for(other=0; other<count; other++)
        {
            if (other != boidID)
            {
                d = state->pos(other) - state->pos(boidID);
                rangeWeight = calcRangeWeight( d.magnitude() );
                visibility = calcVisibilityWeight( d, state->vel(boidID) );

                avoidAccel += calcCollisionAvoidance(state, 
                            boidID, other, rangeWeight, visibility);
                velAccel += calcVelocityMatching(state,
                            boidID, other, rangeWeight, visibility);
                centerAccel += calcCentering(state,
                            boidID, other, rangeWeight, visibility);
            }
        }


      //Modifier to boids: Limit the acceleration.
        if ( avoidAccel.magnitude() > maxAccel )
        {
            avoidAccel = avoidAccel * (maxAccel / avoidAccel.magnitude());    //Scale down magnitude to maxAccel.
            velAccel.set(0,0,0);         //Already maxed out the total accel, so set these to 0 vectors.
            centerAccel.set(0,0,0);
        }
        else
        {
            residual = maxAccel - avoidAccel.magnitude();   //New threshold for the other accels to stay below.
            if ( velAccel.magnitude() > residual )
            {
                velAccel = velAccel * (residual / velAccel.magnitude());
                centerAccel.set(0,0,0);
            }
            else
            {
                residual = residual - velAccel.magnitude();
                if ( centerAccel.magnitude() > residual )
                { 
                    centerAccel = centerAccel * (residual / centerAccel.magnitude());
                } 
            }
        }

        totalAccel = avoidAccel + velAccel + centerAccel; 
        return totalAccel;
    }

  //---------------------------------
  private:
    //Ranges from boid of interest are radii r1 and r2.
    //dist is the distance between the current boid and one other boid.
    //r1 is the class variable range, and r2 is r1+rangeRamp.
    double calcRangeWeight(double dist)
    {
        double r1, r2, numer, denom, zeroEps(0.000001);
        r1 = range;
        r2 = r1 + rangeRamp*r1;

        if (dist < r1 )         //Full effect in full range.
            { return 1.0; }
        else if (dist > r2)     //No effect if outside of range.
            { return 0.0; }
        else                    //Ratio between the radii. Make sure not to divide by zero.
        { 
            numer = r2-dist;
            denom = r2-r1;  
         
            if (denom > zeroEps)
                { return numer/denom; }
            else 
                { return 0.0; }
        }  

    }

  //--------------------------
    //dirIJ: Direction and magnitude between boids i and j
    //vel: The velocity of the current boid i.
    //theta1: Angle of the primary vision of boid i (to either side of its heading).
    //theta2: Angle of the peripheral vision of boid i.
    double calcVisibilityWeight(pba::Vector dirIJ, pba::Vector vel)
    {
        double dist, cosThetaIJ, theta1, theta2, zeroEps(0.000001);
        double thetaIJ, halfT1, halfT2, numer, denom;

        dist = dirIJ.magnitude(); 
        theta1 = fieldOfView;
        theta2 = theta1 + fovRamp*theta1;

      //They're way too close and are definitely visible to each other
      //  when the distance is too close to 0. Return here as to not 
      //  divide by zero in finding cosThetaIJ!
        if (dist < zeroEps)
        { return 1.0; }

      //Cos of the angle between the two vectors is the dot product (*) of their normalized versions.
        vel.normalize();     //void function
        dirIJ.normalize();
        thetaIJ = acos(vel * dirIJ);

        halfT1 = theta1/2.0;
        halfT2 = theta2/2.0;
        
      //Full visibility, within primary vision.
        if (thetaIJ <= halfT1)
            { return 1.0; }
 
      //No visibility, outside of Periphery (which is also the name of a metal band \m/).
        else if (thetaIJ >= halfT2)
            { return 0.0; }

        else
        {
            numer = halfT2 - thetaIJ; 
            denom = halfT2 - halfT1;

         //Make sure not to divide by zero.
            if ((denom > zeroEps) && (numer >zeroEps))
                { return numer/denom; }
            else 
                { return 0.0; }
        }

    }

  //--------------------------
    //Acceleration on boid "boid" due to collision avoidance from all other boids in range and visibility.
    //boidI is the current boid ID being compared with every other boidJ.
    //range and vis are the range and visibility constraints calculated per loop of the sum in calcTotalAccel().
    //This acceleration gets smaller as distance between I and J grows but causes repulsion as the distance shrinks.
    pba::Vector calcCollisionAvoidance(pba::DynamicalState &state, 
        int boidI, int boidJ, 
        double rangeWeight, double visWeight)
    {
        pba::Vector d, accel;
        double dist, zeroEps(0.000001);
        
        d = state->pos(boidJ) - state->pos(boidI);
        dist = d.magnitude();

      //If the distance is too close to zero, make sure to not divide by zero.
        if (dist < zeroEps)
            { dist = zeroEps; } 

        accel = -avoidance * (d / (dist*dist) ); 
        accel *= (rangeWeight * visWeight);
        
        return accel;
    }
  //--------------------------
  //If there is a difference in velocities, accelerate to get vel(I) to match vel(J).
  //Returns (0,0,0) if velocities of the two boids are the same.
    pba::Vector calcVelocityMatching(pba::DynamicalState &state, 
        int boidI, int boidJ,
        double rangeWeight, double visWeight)
    {
        pba::Vector accel;

        accel = velocityMatching * (state->vel(boidJ) - state->vel(boidI));
        accel *= (rangeWeight * visWeight);

        return accel;
    }

  //--------------------------
  //Centering is the desire to stick together. Acts like a spring trying to attract the two boids together.
    pba::Vector calcCentering(pba::DynamicalState &state,
        int boidI, int boidJ,
        double rangeWeight, double visWeight)
    {
        pba::Vector accel, d;

        d = state->pos(boidJ) - state->pos(boidI);

        accel = centering * d;
        accel *= (rangeWeight * visWeight);

        return accel;
    }

  //--------------------------
  //Private data
    double avoidance, velocityMatching; 
    double centering, maxAccel;
    double range, rangeRamp;
    double fieldOfView, fovRamp;

};  //End class



}  //End namespace


#endif
