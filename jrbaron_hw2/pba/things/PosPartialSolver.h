//-------------------------------------------------------
//
//  PosPartialSolver.h
//  A partial solver class for position. Determines and handles 
//      collision based on Mesh information passed in. 
//    S(x)(dt) {x,v} = {(x + dt*v), v}       //Update pos by a timestep of vel.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __POSPARTIAL_H__
#define __POSPARTIAL_H__

#include <cmath>
#include <vector>
#include <algorithm>  //For std:max_element()

namespace pba
{

class PosPartialSolver : public SolverThing
{
  public:
    PosPartialSolver() :
        SolverThing("PosPartialSolver")
    {

    };

    ~PosPartialSolver() {};

  //--------------------------------------
  //Inherited function. Should be called by other solvers 
  //    when getting information down to this level.
  void setRestitution(double r)
     { restitution = r; } 

  //--------------------------------------
  //Inherited function
  void setStickiness(double s)
     { stickiness = s; } 

  //--------------------------------------
  //collide: Reference to a bool signal if there's a collision or not.
  //x: Particle position (If getting a timestep to trace back 
  //    to intersection, this is the *next* position.)
  //v: Current velocity of the particle.
  //Returns a timestep to walk back to the time of intersection between a particle and a plane.
  //    If no collision, return -1. (Timestep if returned should be in [0,1].)
    double detectCollWithPlane( bool &collide, Vector x, Vector v, double dt, Tri tri )
    {
        double q0, q1, dt_i; //eps(0.00000001);
        Vector np, edge0, edge1, xp; 

        xp = tri.v0;    //Any of the three triangle verts are a point on the plane.
        edge0 = tri.v1 - tri.v0;
        edge1 = tri.v2 - tri.v0;

      //^ for cross product
        np = edge0 ^ edge1;
        np.normalize(); 

      // * for dot product
        q0 = (x - xp) * np;         //Plane normal projected by distance between curr pos and pos on plane.
        q1 = (x-(v*dt) - xp) * np;  //With the previous position
        //printf("q0=%f, q1=%f\n", q0, q1);


      //No collision if q0 == 0. 
        if ( q0 == 0.0 )
        { 
            collide = false;
            return -1;  
        }

     //If on plane at prev timestep (q1 == 0), then collision at the full time to go backwards, dt.
       if ( q1 == 0.0 )
       {
           collide = true;
           return dt;
       } 

      //No collision here also.
        if ( q0*q1 > 0.0 )
        { 
            collide = false;
            return -1; 
        }

      //Otherwise, so far there's a collision.
      //dt_i is the ratio of the new pos to plane projected
      //   onto plane normal, and the vel projected onto plane normal.
        dt_i = q0 / (v*np);
        //printf("dt_i = %f\n", dt_i);

      //Sanity check in case values had gotten too close to zero:
        if (dt*dt_i < 0.0)
        {  
            collide = false;
            return -1; 
        }

        if (dt_i > dt)
        { 
            collide = false;
            return -1; 
        }

        //if ( abs((dt-dt_i)/dt) <= eps )    //If collision happened really soon when x is "on" the plane.
        if ( ((dt-dt_i)/dt) == 0.0 )
        { 
            collide = false;
            return -1; 
        }

        collide = true;
        return dt_i;
    };

  //--------------------------------------
  //Called after detectCollWithPlane() if there was a collision.
  //Now, see if that collision was with the triangle of interest.
    bool detectCollWithTri( Vector xi, Tri tri )
    {
        Vector edge0, edge1, cross0, cross1;
        double a, b, mag0, mag1;           //Barycentric coordinates
        
        edge1 = tri.v2 - tri.v0;
        edge0 = tri.v1 - tri.v0;

        cross0 = edge1^edge0;
        cross1 = edge0^edge1;
        mag0 = cross0.magnitude();
        mag1 = cross1.magnitude();
        
       //^ for cross product, * for dot product
       //No collision if barycentric coords and their sums are not within [0,1] range.
        a = cross0 * (edge1 ^ (xi - tri.v0));
        a = a / (mag0*mag0);
        if ( (a<0) || (a>1) )
            { return false; } 

        b = cross1 * (edge0 ^ (xi - tri.v0));
        b = b / (mag1*mag1);
        if ( (b<0) || (b>1) )
            { return false; } 

        if ( (a+b < 0) || (a+b > 1) )
            { return false; } 

       return true;
    };

  //--------------------------------------
  //collide: Reference to a bool signal if there's a collision or not.
  //x: Current particle position
  //xn: Next timestep particle position.
  //v: Current velocity of the particle.
    double detectCollision( bool &collide, Vector x, Vector xn, Vector v, double dt, Tri tri )
    {
        Vector xi;
        double dt_i;
        dt_i = detectCollWithPlane(collide, xn, v, dt, tri);   //xi is where the particle collides with a plane made out of the triangle.

        if (collide)                 //Now see if this position is actually within the triangle. 
        {
            //printf("TRUE: Collided with PLANE.\n");

            xi = xn - v*dt_i;
           // collide = detectCollWithTri(xi, tri);

            if (collide)
            { 
                //printf("TRUE: Collided with TRI.\n");
                return dt_i; 
            }
            else
            {
                //printf("FALSE: No collide with TRI.\n");
                return -1; 
            }
        }
        else
        { 
            //printf("FALSE: No collide with PLANE.\n");
            return -1;  
        }

    };

  //--------------------------------------
  //For each particle of the system, determine its position at the next timestep.
  //    Use this position and compare with each triangle of the Mesh to detect and 
  //    handle a collision if necessary.
    void solve(double dt, DynamicalState &state, Mesh &mesh, BoidsForces &boidsForces)
    {
        Vector x, v, xi, nextX, vr, v_perp, np, edge0, edge1;
        Color col, newColor;
        Tri *tris, hitTri;
        int p, tr, i, numParticles, numTris;
        double dt_i, max_dt_i;
        bool collide, aCollision=false;
        
        numParticles = state->nb(); 
        numTris = mesh.getNumFaces(); 
        tris = mesh.getTris();
        for (p=0; p<numParticles; p++)
        {
            x = state->pos(p);
            v = state->vel(p);
            col = state->ci(p);
            nextX = x+dt*v;

            max_dt_i = -2.0;
            for (tr=0; tr<numTris; tr++)
            {
                dt_i = detectCollision( collide, x, nextX, v, dt, tris[tr] );
                if (collide)
                { 
                    //printf("p=%d, tr=%d, dt_i: %f\n", p, tr, dt_i);
                    if (dt_i > max_dt_i)
                    {
                        max_dt_i = dt_i;
                        hitTri = tris[tr];
                    }
                    aCollision = true;
                }
            }


         //Handle collision for largest dt_i (first that occurred, closest to x). 
            if (aCollision)
            { 
                edge1 = hitTri.v2 - hitTri.v0;
                edge0 = hitTri.v1 - hitTri.v0;
                np = edge0 ^ edge1;
                np.normalize();
                v_perp = v - np*(np*v);

            //Compute reflected velocity as inelastic reflection.
                vr = stickiness*v_perp - restitution*np*(np*v);

                xi = nextX - max_dt_i*v;       //Pos at intersection is tracing back from the next time step pos by the intersection timestep found.
                nextX = xi + max_dt_i*vr;      //Change from intersection pos in dir of reflection

                state->set_pos(p, nextX);
                state->set_vel(p, vr);
            }

          //Else if no collision at all.
            else
            {
                state->set_pos(p, nextX);
            }

            aCollision = false;
            max_dt_i = -2.0;
        }  //End for each particle
   
    };

  //--------------------------------------
  private:


};  //End class


pba::Solver createPosPartialSolver()
{
    return Solver( new pba::PosPartialSolver() );
}


}  //End namespace

#endif
