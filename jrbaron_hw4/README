Modified by Jessica Baron for CPSC8170, Fall 2017.
Homework 4:
  Assemble a cloth plane that collides with a triangulated plane and falls through a hole. 

To build and run pba, do the following

Build:
-------

make
make viewer
make stuff


Run:
--------

You must first set the environment variable PYTHONPATH:

export PYTHONPATH=<full path to the directory holding the pba directory>

----
The following runs Jessica Baron's homework 4 project:

python/launch_HW4.py (from the pba directory)

or

./launch_HW4.py (from the pba/python directory)

Optional parameters can be given to the script as:
  1. Width (in vertices, along x-axis) of plane
  2. Depth (in vertices, along z-axis) of plane
  3. Edge length (space between the vertices)
  4. Y-coordinate of the plane
  5. A percentage [0.0, 1.0] defining the hole in the middle
       of the plane in respect to the width and height. 
  6. 1 (true) or 0 (false) for whether the collision surface
       is a plane or the default loaded OBJ.
 
For example:
  python/launch_HW4.py 50 50 0.5 -1 0.75 1  
(A 30x30 cloth plane with 0.5 edge lengths, starting at a y-coord of -1, to collide with a similarly generated plane with a hole that's 75% of the full width and 75% of the depth.)

or

  python/launch_HW4.py 20 20 0.1 2 5 0
(A 20x20 cloth plane with 0.1 edge lengths, starting at a y-coord of 2, to collide with the default loaded OBJ.  (Param 5 doesn't matter with the last set to 0/false for OBJ loading.)
