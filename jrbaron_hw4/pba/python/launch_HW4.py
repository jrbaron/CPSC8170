#!/usr/bin/python

import os
import sys
import pba.swig.PbaViewer as pbav
import pba.swig.PbaThings as things

viewer = pbav.CreateViewer()

args = pbav.StringArray()
for s in sys.argv:
	args.push_back(s)

if len(sys.argv) == 7:
    print("Making system using params.")
    width = int(sys.argv[1])
    depth = int(sys.argv[2])
    edgeLength = float(sys.argv[3])
    y_coord = float(sys.argv[4])
    holePercent = float(sys.argv[5])
    makePlane = int(sys.argv[6])
    if makePlane == 1:
        makePlane = True 
    else:
        makePlane = False
    print("makePlane:", makePlane)
    system = things.makeClothCollisionSystem(width, depth, edgeLength, y_coord, holePercent, makePlane)

else:
    print("Making default system.")
    system = things.makeClothCollisionSystem(20, 20, 0.75, -2.0, 0.75, True)

viewer.AddThing(system)

viewer.Init(args)
viewer.MainLoop()

