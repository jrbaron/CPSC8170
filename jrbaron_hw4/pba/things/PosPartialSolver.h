//-------------------------------------------------------
//
//  PosPartialSolver.h
//  A partial solver class for position. Determines and handles 
//      collision based on Mesh information passed in. 
//    S(x)(dt) {x,v} = {(x + dt*v), v}       //Update pos by a timestep of vel.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __POSPARTIAL_H__
#define __POSPARTIAL_H__

#include <cmath>
#include <vector>
#include <algorithm>  //For std:max_element()

namespace pba
{

//class PosPartialSolver : public SolverThing
class PosPartialSolver
{
  public:
    PosPartialSolver() :
      restitution(1.0),
      stickiness(1.0)
    { };

    ~PosPartialSolver() {};

  //--------------------------------------
  //Inherited function. Should be called by other solvers 
  //    when getting information down to this level.
  void setRestitution(double r)
     { restitution = r; } 

  //--------------------------------------
  //Inherited function
  void setStickiness(double s)
     { stickiness = s; } 

  //--------------------------------------
  //See if a collision was with the triangle of interest.
    bool detectCollWithTri( Vector xi, Tri tri )
    {
        Vector edge0, edge1, cross0, cross1;
        double a, b, mag0, mag1;           //Barycentric coordinates
        
        edge1 = tri.v2 - tri.v0;
        edge0 = tri.v1 - tri.v0;

        cross0 = edge1^edge0;
        cross1 = edge0^edge1;
        mag0 = cross0.magnitude();
        mag1 = cross1.magnitude();
        
       //^ for cross product, * for dot product
       //No collision if barycentric coords and their sums are not within [0,1] range.
        a = cross0 * (edge1 ^ (xi - tri.v0));
        a = a / (mag0*mag0);
        if ( (a<0) || (a>1) )
            { return false; } 

        b = cross1 * (edge0 ^ (xi - tri.v0));
        b = b / (mag1*mag1);
        if ( (b<0) || (b>1) )
            { return false; } 

        if ( (a+b < 0) || (a+b > 1) )
            { return false; } 

       return true;
    };

  //--------------------------------------
  Vector getTriNormal(Tri tri)
  {
      Vector e0, e1, n;
      e0 = tri.v1 - tri.v0;
      e1 = tri.v2 - tri.v0;
      n = e0 ^ e1;
      n.normalize();
      return n;
  }

  //--------------------------------------
  void solveSB(double dt, SoftBodyState &state, Mesh &mesh)
  {
      int numParticles, numTris, p, tr, hitCount;
      double f0, f1, dt_i, max_dt_i(0.0);                //max_di_i is initialized with first collision.
      Vector xi, vi, nextX, xp, np, x_coll, vr, v_perp;
      Tri *tris, tri, hitTri;
      KDTreeNode *kdNode;
      std::vector<Tri> kdTris;
      bool collide = true; 
      Color col;

      numParticles = state->nb();
      //numTris = mesh.getNumFaces(); 
      //tris = mesh.getTris();
      hitCount = 0;

      for (p=0; p<numParticles; p++)
      {
          xi = state->pos(p);
          vi = state->vel(p);
          col = state->ci(p);

          nextX = xi + dt*vi;

          kdNode = mesh.getKDNodeOfPoint(nextX);
          if (kdNode != NULL) 
          {
            kdTris = kdNode->triList;
            numTris = (int)kdTris.size();
            //printf("Particle p=%d checked with %d tris.\n", p, kdTris.size());
            //printf("kdNode->level = %d\n", kdNode->level);
            for (tr=0; tr<numTris; tr++)
            {
                tri = kdTris[tr]; 
                xp = tri.v0;  
                np = getTriNormal(tri);

                f0 = (nextX - xp) * np;       //Using the pos *after* the motion.
                f1 = (xi - xp) * np;          //Using the pos *before* the motion.

                if ( f0 == 0.0 )              //No collision if next pos exactly on plane.
                  { collide = false; }

                if ( f1 == 0.0 )              //Collision at full timestep backwards if init pos is on the plane.
                  { dt_i = dt; }

                if ( f0*f1 > 0.0 )
                  { collide = false; }

                if (collide)                 //If collision so far, find dt_i within (0,dt).
                {
                    dt_i = f0 / (vi*np);
                  //Sanity check in case values had gotten out of bounds. 
                    if (dt*dt_i < 0.0)
                       { collide = false; }

                    if (fabs(dt_i) > fabs(dt))   //Compare to abs values in case negative (6th order solver)!
                       { collide = false; }

                  //If collision happened really soon when x is "on" the plane.
                    if ( ((dt-dt_i)/dt) == 0.0 )
                      { collide = false; }


                    if (collide)               //If there's still a valid collision, update the current first collision with this particle.
                    {
                        x_coll = nextX - dt_i*vi;
                        if (detectCollWithTri(x_coll, tri))
                        {
                          //Remember that dt can be positive or negative (with Sixth Order solver).
                            if ( fabs(dt_i) >= fabs(max_dt_i) ) 
                            {
                              max_dt_i = dt_i;
                              hitTri = kdTris[tr];
                              hitCount++;
                            }
                        }
                    }
                }
            } //End for triangles


         //Handle collision for largest dt_i (first that occurred, closest to x). 
          if (hitCount > 0)
          {
              np = getTriNormal(hitTri);
              v_perp = vi - np*(np*vi);

            //Compute reflected velocity as inelastic reflection.
              vr = stickiness*v_perp - restitution*np*(np*vi);

              x_coll = nextX - max_dt_i*vi;       //Pos at intersection is tracing back from the next time step pos by the intersection timestep found.
              nextX = x_coll + max_dt_i*vr;      //Change from intersection pos in dir of reflection

              state->set_pos(p, nextX);
              state->set_vel(p, vr);
          }

        //Else if no collision at all and no need to change vel for the (nonexistent) reflection.
          else
              { state->set_pos(p, nextX); }

          collide = true;
          max_dt_i = 0.0;

          }  //End if kdNode != NULL
          else
              { state->set_pos(p, nextX); }
      }  //End for each particle

  };

  //--------------------------------------
  private:
    double restitution;
    double stickiness;

};  //End class

}  //End namespace

#endif
