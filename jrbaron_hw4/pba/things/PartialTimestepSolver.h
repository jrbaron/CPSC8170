//-------------------------------------------------------
//
//  PartialTimestepSolver.h
//  A solver class that breaks up the given timestep by some factor
//    to call another solver with multiple times.
//    This is beneficial so the simulation calculations can occur 
//    between steps that are actually displayed (every solve() call
//    with dt by the System vs. the multiple sub-solve() calls here).
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class PartialTimestepSolver : public SolverThing
{
  public:
    PartialTimestepSolver(Solver solv, double timestep, int div) :
        SolverThing("PartialTimestep"),
        currSolver(solv),
        dt(timestep),
        divides(div)
    {
      std::cout << "Created PartialTimestep solver using " << currSolver->getName() << ".\n";
    };

    ~PartialTimestepSolver() {};

  //--------------------------------------
  const std::string& getName()
  {
      return name+":"+currSolver->getName();
  }

  //--------------------------------------
  //Inherited method.
  void setGravMagnitude(double g)
     { currSolver->setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
  void setRestitution(double r)
     { currSolver->setRestitution(r); }

  //--------------------------------------
  //Inherited method.
  void setStickiness(double s)
     { currSolver->setStickiness(s); }

  //--------------------------------------
  void setSpringStrength(double s)
     { currSolver->setSpringStrength(s); }

  //--------------------------------------
  void setFrictionStrength(double s)
     { currSolver->setFrictionStrength(s); }

  //--------------------------------------
  void setSubstepDivides(int d)
      { divides = d; }

  //--------------------------------------
    void solveSB(double dt, SoftBodyState &state, Mesh &mesh)
    {
        double sm_dt;
        int i;

        sm_dt = dt/divides; 
        for(i=0; i<divides; i++) 
            { currSolver->solveSB(sm_dt, state, mesh); }
    };

  //--------------------------------------
  private:
    Solver currSolver;
    double dt; 
    int divides; 

};  //End class


pba::Solver createPartialTimestepSolver(Solver solv, double timestep, int div)
{
    return Solver( new pba::PartialTimestepSolver(solv, timestep, div) );
}

}  //End namespace
