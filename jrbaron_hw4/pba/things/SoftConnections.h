
#ifndef __SOFTCONNECTIONS_H__
#define __SOFTCONNECTIONS_H__

//These are in their own file for easier access by multiple classes.

//-----------------------------------
//2 vertices with an initial distance apart.
struct SoftEdge
{
    int i, j;
    double length;   //Initial length of the edge (dist between the pts)
};

//-----------------------------------
//3 vertices with an initial area.
struct SoftTriangle
{
    int i, j, k; 
    double area;
};

//-----------------------------------
//2 triangles with a shared edge (and remember they're in 3D space).
//(Currently unused in HW4.)
struct SoftBendable
{
    int i, j, k, l;
    double theta;    //Initial theta from crossing the normals of the left and right triangles.
};

//-----------------------------------

#endif
