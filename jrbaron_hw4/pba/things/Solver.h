//-------------------------------------------------------
//
//  Solver.h
//  A base solver class from which all solvers are derived.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SOLVERTHING_H__
#define __SOLVERTHING_H__

#include "Mesh.h"
#include "SoftBodyState.h"
#include <iostream>

namespace pba
{

class SolverThing
{
  public:
    SolverThing( const std::string nam="SolverThing_NoName" );
    virtual ~SolverThing() {};
    virtual void solveSB(double dt, SoftBodyState &state, Mesh &mesh) {};
    virtual void setGravMagnitude(double g) {}; 
    virtual void setRestitution(double r) {}; 
    virtual void setStickiness(double s) {};
    virtual void setSpringStrength(double s) {};
    virtual void setFrictionStrength(double s) {};
    virtual void setSubstepDivides(int d);
    const std::string& getName();

  protected:
    std::string name;

};  //End class


typedef std::shared_ptr<SolverThing> Solver;

}  //End namespace


#endif
