//-------------------------------------------------------
//
//  VelPartialSolver.h
//  A partial solver class for velocity.
//     S(v)(dt) {x,v} = {x, (v + f(x,v)/m *dt)}   //Update vel by a timestep of accel.
//  Also implements downward gravity.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class VelPartialSolver
{
  public:
    VelPartialSolver() :
      gravMagnitude(1.0),
      springStrength(1.0),
      frictionStrength(1.0)
    { };

    ~VelPartialSolver() {};

  //--------------------------------------
    void setGravMagnitude(double g)
        { gravMagnitude = g; };

  //--------------------------------------
    void setSpringStrength(double s)
        { springStrength = s; };

  //--------------------------------------
    void setFrictionStrength(double s)
        { frictionStrength = s; };

  //--------------------------------------
    void solveSB(double dt, SoftBodyState &state, Mesh &mesh)
    {
        Vector forces, x, v;
        Vector g(0, gravMagnitude*-9.8, 0);     
        double m;
        int count, i;

        count = state->nb();
        for (i=0; i<count; i++)
        {
            x = state->pos(i);
            v = state->vel(i);
            m = state->mass(i);

            forces = m*g;   //The assignment here resets the force for each particle to gravity.

          //Strut force by itself or plus the area force is more prone to numerical error than area force alone, so area force is the default.
            forces += state->calcStrutForce(i, springStrength, frictionStrength);    
          //  forces += state->calcTriAreaForce(i, springStrength, frictionStrength);

            state->set_vel( i, v + (dt*forces)/m );
        }

    };

  //--------------------------------------
  private:
    double gravMagnitude, springStrength, frictionStrength;

};  //End class

}  //End namespace
