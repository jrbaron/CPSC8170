//-------------------------------------------------------
//
//  ClothCollisionSystem.h
//  PbaThing for a system handling soft bodies, particularly
//    a plane as a "cloth" to collide with a rigid plane and
//    fall through a hole.
//
//  Jessica Baron 
//  CPSC 8170: Homework 3
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#define PI 3.14159265  

#include "PbaThing.h"

#include "Solver.h"
#include "Mesh.h"
#include "SoftBodyState.h"

#include "PosPartialSolver.h"
#include "VelPartialSolver.h"
#include "LeapfrogSolver.h"
#include "SixthOrderSolver.h"
#include "PartialTimestepSolver.h"

#include "Vector.h"
#include "Color.h"
#include <iostream>

#ifdef __APPLE__
  #include <OpenGL/gl.h>   // OpenGL itself.
  #include <OpenGL/glu.h>  // GLU support library.
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>   // OpenGL itself.
  #include <GL/glu.h>  // GLU support library.
  #include <GL/glut.h> // GLUT support library.
#endif

namespace pba
{


class ClothCollisionSystem: public PbaThingyDingy
{
  public:

    //--------------------------------------------------------
    ClothCollisionSystem(int width, int depth, double edgeLength, 
            double y_coord, double holePercent, bool makePlane,
            const std::string nam="ClothCollisionSystem") :
        PbaThingyDingy(nam),
        numParticles(-1),      //Should be set by the bodyMesh.
        subTimestep(4),
        solver( pba::createPartialTimestepSolver(pba::createSixthOrderSolver(), dt, subTimestep) ),
        state( pba::createSoftBodyState("Soft-body mesh's state") ),
        collMeshState( pba::createSoftBodyState("Collision mesh's state") ),
    //Mesh(vertices wide (x-axis), vertices deep (z-axis), length of edge between two neighbors,
    // num of edges per face (tri or quad but regardless will be *displayed* as tris), y-coord)
        mesh(NULL),
        bodyMesh( new Mesh(width, depth, 0, 0, edgeLength, 4, y_coord+abs(y_coord*0.5)) ),  
        gravMag(5.0), 
        restitution(0.25),  
        stickiness(1.0),      //The goal is to get the "cloth" through the hole, so it shouldn't cling to the plane much.
        springStrength(width*3),
        frictionStrength(width*3)
    {
        std::cout << "Making a " << name << std::endl; 

        if (makePlane)
        { 
            mesh = new Mesh(width, depth, int(width*holePercent),
                  int(depth*holePercent), edgeLength, 3, y_coord); 
        } 
        else
        { 
            mesh = new Mesh("models/smaller/smallsphere.obj", 3);
        }

        int num;
        dt = 0.005;    //Start very small.

        numParticles = bodyMesh->getNumVertices();
        state->add(numParticles);	
        bodyMesh->setStatesSoftConnections(state);
        setupState(state, bodyMesh, 3.5);   //Last param is a modifier in the color setting.

        if (mesh->getIsPlane())
        {
           num = mesh->getNumVertices();
           collMeshState->add(num);   
           mesh->setStatesSoftConnections(collMeshState);
           setupState(collMeshState, mesh, 1.0);
        }

        solver->setGravMagnitude(gravMag);
        solver->setRestitution(restitution);
        solver->setStickiness(stickiness);

        std::cout << name << " constructed\n";
    };

//--------------------------------------------------------
    ~ClothCollisionSystem()
    {
        delete mesh;
        delete bodyMesh;
    };
   
    //--------------------------------------------------------
    void Init( const std::vector<std::string>& args )
    {

    }

    //--------------------------------------------------------
    //c alters color percentages since this method is used by both meshes.
    void setupState(pba::SoftBodyState &s, Mesh *m, double c)
    {
        int i;
        double per, random, yellow; 
        Vector *meshVerts;

        meshVerts = m->getPbaVectors();

        random = (rand()%100)/100.0;
        for (i=0; i<numParticles; i++)
        {
            per = (i*1.0)/numParticles;
            yellow = ((1-per)*0.5)+0.5;
            s->set_id(i, i);
            s->set_pos(i, meshVerts[i]);	
            s->set_vel(i, Vector());	
            s->set_mass(i, 1.0);
            s->set_ci(i, pba::Color(random*c*0.3, yellow*c*0.7, yellow*c*2, 0));
        }

    }
   
    //--------------------------------------------------------
    // Callback functions
    void Display()
    {
        Vector pos;
        std::vector<SoftEdge> pairs;
        SoftEdge pair;
        int p, i, j, cnt;

    //Draw "cloth" mesh.
        pairs = state->getSoftEdges();
        cnt = pairs.size();
        glBegin(GL_LINES);
        //glPointSize(5.0);   //See as particles for testing.
        //glBegin(GL_POINTS);
        for(p=0; p<cnt; p++)
        {
            pair = pairs[p];
            i = pair.i;
            j = pair.j;
            glColor3f( state->ci(i).red(), state->ci(i).green(), state->ci(i).blue() ); 
            pos = state->pos(pair.i);
            glVertex3f( pos.X(), pos.Y(), pos.Z() );
            pos = state->pos(pair.j);
            glVertex3f( pos.X(), pos.Y(), pos.Z() );
        }
        glEnd();


    //Draw mesh to collide with.
        if (mesh->getIsPlane())
        {
           pairs = collMeshState->getSoftEdges();
           cnt = pairs.size();
           glBegin(GL_LINES);
           for(p=0; p<cnt; p++)
           {
               pair = pairs[p];
               i = pair.i;
               j = pair.j;
               glColor3f( collMeshState->ci(i).red(), collMeshState->ci(i).green(), collMeshState->ci(i).blue() ); 
               pos = collMeshState->pos(pair.i);
               glVertex3f( pos.X(), pos.Y(), pos.Z() );
               pos = collMeshState->pos(pair.j);
               glVertex3f( pos.X(), pos.Y(), pos.Z() );
           }
           glEnd();
       }
       else   //if loaded from OBJ
       {
           glColor3f( 0.3, 0.4, 0.6 );
           glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
           glVertexPointer( 3, GL_FLOAT, 3*sizeof(GLfloat), mesh->getVertexArray() );
           glEnableClientState(GL_VERTEX_ARRAY);
           glDrawArrays( GL_TRIANGLES, 0, mesh->getNumFaces()*3 );
       }

    };

    //-------------------------------
    //PbaThingyDingy::idle() handles calls to solve(), and this class already created a dt.
    void solve()
    {
        solver->solveSB(dt, state, *mesh);
    };

    //-------------------------------
    void Reset()
    {
     //Currently no way of cleaning up state and resetting num of particles.
     //Just move all particles back to origin with new starting positions and velocities.
        setupState(state, bodyMesh, 3.5);
    };

    //-------------------------------
    void Usage()
    {
        PbaThingyDingy::Usage();

        std::cout << "=== " << name << " ===\n";
        std::cout << "p:     Print current parameter values.\n";
        std::cout << "b/B:   Reduce/increase the number of timestep divisions.\n";
        std::cout << "u/U:   Reduce/increase the strength of the struts' spring force.\n";
        std::cout << "i/I:   Reduce/increase the strength of the struts' friction force.\n";
        std::cout << "g/G:     Reduce/increase magnitude of gravity.\n";
        std::cout << "c/C:     Reduce/increase coefficient of restitution in collisions.\n";
        std::cout << "s/S:     Reduce/increase coefficient of stickiness in collisions.\n";
        std::cout << "1/2/3/4:     Change solver to (1) Leapfrog, (2) Sixth Order using Leapfrog,\n" <<
          "\t\t(3) Partial Timestep using Leapfrog, or (4) Partial Timestep using Sixth Order.\n";
    };

    //-------------------------------
    void Keyboard(unsigned char key, int x, int y)
    {
        PbaThingyDingy::Keyboard(key, x, y);

        if (key == 'u')
        {
            springStrength -= (springStrength*0.1);
            if (springStrength <= 0.01) { springStrength = 0.0; }  
            solver->setSpringStrength(springStrength);
            std::cout << "Strut spring strength: " << springStrength << ".\n";
        }
        else if (key == 'U')
        {
            if (springStrength <= 0.01) { springStrength = 0.1; }  
            springStrength += (springStrength*1.1);
            solver->setSpringStrength(springStrength);
            std::cout << "Strut spring strength: " << springStrength << ".\n";
        }

        if (key == 'i')
        {
            frictionStrength -= (frictionStrength*0.1);
            if (frictionStrength <= 0.01) { frictionStrength = 0.0; }  
            solver->setFrictionStrength(frictionStrength);
            std::cout << "Strut friction strength: " << frictionStrength << ".\n";
        }
        else if (key == 'I')
        {
            if (frictionStrength <= 0.01) { frictionStrength = 0.1; }  
            frictionStrength += (frictionStrength*1.1);
            solver->setFrictionStrength(frictionStrength);
            std::cout << "Strut friction strength: " << frictionStrength << ".\n";
        }

      //Change which solver is being used.
        if (key == '1')
        { 
            solver = pba::createLeapfrogSolver();
            Reset();
            std::cout << "Solver changed to Leapfrog.\n";
        }
        else if (key == '2')
        { 
            solver = pba::createSixthOrderSolver();
            Reset();
            std::cout << "Solver changed to Sixth Order using Leapfrog.\n";
        }
        else if (key == '3')
        { 
            solver = pba::createPartialTimestepSolver( pba::createLeapfrogSolver(), dt, subTimestep );
            Reset();
            std::cout << "Solver changed to Partial Timestep using Leapfrog.\n";
        }
        else if (key == '4')
        { 
            solver = pba::createPartialTimestepSolver( pba::createSixthOrderSolver(), dt, subTimestep );
            Reset();
            std::cout << "Solver changed to Partial Timestep using Sixth Order.\n";
        }

     //Change magnitude of gravity but keep positive.
        if (key == 'g')
        {
            gravMag -= 0.01;
            if (gravMag <= 0.01) { gravMag = 0.0; }         //Prevent from being negative.

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }
        else if (key == 'G')
        {
            gravMag += 0.01;

            solver->setGravMagnitude(gravMag);
            std::cout << "Gravity magnitude: " << gravMag << "\n";
        }

   //Change stickiness but keep in range [0.0, 1.0].
        if (key == 's')
        {
            stickiness -= 0.01;
            if (stickiness < 0) { stickiness = 0.0; }    //Prevent from being negative.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }
        else if (key == 'S')
        {
            stickiness += 0.01;
            if (stickiness > 1) { stickiness = 1.0; }    //Prevent from exceeding 1.0.

            solver->setStickiness(stickiness);
            std::cout << "Coeff. of stickiness: " << stickiness << "\n";
        }

     //Change restitution but keep in range [0.0, 1.0].
        if (key == 'c')
        {
            restitution -= 0.01;
            if (restitution < 0) { restitution = 0.0; }    //Prevent from being negative.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }
        else if (key == 'C')
        {
            restitution += 0.01;
            if (restitution > 1) { restitution = 1.0; }    //Prevent from exceeding 1.0.

            solver->setRestitution(restitution);
            std::cout << "Coeff. of restitution: " << restitution << "\n";
        }

     //Change the timestep division but keep greater than 0.
        if (key == 'b')
        {
            subTimestep -= 1;
            if (subTimestep == 0) { subTimestep = 1; } 
            
            solver->setSubstepDivides(subTimestep);
            std::cout << "Timestep divisions: " << subTimestep << "\n";
        }
        else if (key == 'B')
        {
            subTimestep += 1;
            
            solver->setSubstepDivides(subTimestep);
            std::cout << "Timestep divisions: " << subTimestep << "\n";
        }

     //Print current user parameters.
        if (key == 'p')
        {
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
            std::cout << "Solver:\t" << solver->getName() << "\n";
            std::cout << "Timestep:\t" << dt << "\n";
            std::cout << "Timestep divisions:\t" << subTimestep << "\n";
            std::cout << "Gravity magnitude:\t" << gravMag << "\n";
            std::cout << "Coeff. of restitution:\t" << restitution << "\n";
            std::cout << "Coeff. of stickiness:\t" << stickiness << "\n";
            std::cout << "Strut spring strength:\t" << springStrength << "\n";
            std::cout << "Strut friction strength:\t" << frictionStrength << "\n";
            std::cout << "~ ~~ ~ ~~~~~ ~ ~~ ~\n";
        }
    };

  //--------------------------------------------------------
  private:
    int numParticles, subTimestep;
    Solver solver;
    SoftBodyState state;
    SoftBodyState collMeshState;  //To contain positions
    Mesh *mesh, *bodyMesh;
    double gravMag; 
    double restitution, stickiness;
    double springStrength, frictionStrength;

}; 	//End class declaration



//--------------------------------------------------------
pba::PbaThing makeClothCollisionSystem(
    int width, int depth, 
    double edgeLength, double y_coord, 
    double holePercent, bool makePlane)
{
    return PbaThing( new pba::ClothCollisionSystem(
        width, depth, edgeLength, 
        y_coord, holePercent, makePlane ) );
}


}  //End namespace pba



#endif
