//-------------------------------------------------------
//
//  LeapfrogSolver.h
//  A solver class implementing the Leapfrog approach. 
//    S(lf)(dt) = S(x)(dt/2) S(v)(dt/2) S(x)(dt/2) 
//
//    Composite solver of partial solvers on position, velocity, position.
//
//  Jessica Baron 
//  CPSC 8170
//  Fall 2017
//  Clemson University
//
//--------------------------------------------------------

namespace pba
{

class LeapfrogSolver : public SolverThing
{
  public:
    LeapfrogSolver() :
        SolverThing("Leapfrog"),
        posSolver(),
        velSolver()
    { };

    ~LeapfrogSolver() 
    { };

  //--------------------------------------
  //Inherited method.
    void setGravMagnitude(double g)
        { velSolver.setGravMagnitude(g); }

  //--------------------------------------
  //Inherited method.
    void setRestitution(double r)
        { posSolver.setRestitution(r); }

  //--------------------------------------
  //Inherited method.
    void setStickiness(double s)
        { posSolver.setStickiness(s); }

  //--------------------------------------
  void setSpringStrength(double s)
     { velSolver.setSpringStrength(s); }

  //--------------------------------------
  void setFrictionStrength(double s)
     { velSolver.setFrictionStrength(s); }

  //--------------------------------------
    void solveSB(double dt, SoftBodyState &state, Mesh &mesh)
    {
        posSolver.solveSB(dt/2, state, mesh);
        velSolver.solveSB(dt, state, mesh);
        posSolver.solveSB(dt/2, state, mesh);
    };

  //--------------------------------------
  private:
    PosPartialSolver posSolver; 
    VelPartialSolver velSolver; 

};  //End class


pba::Solver createLeapfrogSolver()
{
    return Solver( new pba::LeapfrogSolver() );
}

}  //End namespace
