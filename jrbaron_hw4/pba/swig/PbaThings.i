//Modified by Jessica Baron for CPSC 8170, Fall 2017.

%module PbaThings
%{
#include "ClothCollisionSystem.h"
%}

%include "std_string.i"
%include "std_vector.i"
%template(StringArray) std::vector<std::string>;

%include "ClothCollisionSystem.h"
